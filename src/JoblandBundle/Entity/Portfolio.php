<?php

namespace JoblandBundle\Entity;

/**
 * Portfolio
 */
class Portfolio
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $countryId;

    /**
     * @var string
     */
    private $imageUrl;

    /**
     * @var string
     */
    private $header;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $firstUrl;

    /**
     * @var string
     */
    private $secondUrl;

    /**
     * @var string
     */
    private $thirdUrl;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Portfolio
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Portfolio
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set countryId
     *
     * @param string $countryId
     *
     * @return Portfolio
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return string
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set imageUrl
     *
     * @param string $imageUrl
     *
     * @return Portfolio
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set header
     *
     * @param string $header
     *
     * @return Portfolio
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Portfolio
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set firstUrl
     *
     * @param string $firstUrl
     *
     * @return Portfolio
     */
    public function setFirstUrl($firstUrl)
    {
        $this->firstUrl = $firstUrl;

        return $this;
    }

    /**
     * Get firstUrl
     *
     * @return string
     */
    public function getFirstUrl()
    {
        return $this->firstUrl;
    }

    /**
     * Set secondUrl
     *
     * @param string $secondUrl
     *
     * @return Portfolio
     */
    public function setSecondUrl($secondUrl)
    {
        $this->secondUrl = $secondUrl;

        return $this;
    }

    /**
     * Get secondUrl
     *
     * @return string
     */
    public function getSecondUrl()
    {
        return $this->secondUrl;
    }

    /**
     * Set thirdUrl
     *
     * @param string $thirdUrl
     *
     * @return Portfolio
     */
    public function setThirdUrl($thirdUrl)
    {
        $this->thirdUrl = $thirdUrl;

        return $this;
    }

    /**
     * Get thirdUrl
     *
     * @return string
     */
    public function getThirdUrl()
    {
        return $this->thirdUrl;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $firstFileName;

    /**
     * @var string
     */
    private $secondFileName;

    /**
     * @var string
     */
    private $thirdFileName;


    /**
     * Set firstFileName
     *
     * @param string $firstFileName
     *
     * @return Portfolio
     */
    public function setFirstFileName($firstFileName)
    {
        $this->firstFileName = $firstFileName;

        return $this;
    }

    /**
     * Get firstFileName
     *
     * @return string
     */
    public function getFirstFileName()
    {
        return $this->firstFileName;
    }

    /**
     * Set secondFileName
     *
     * @param string $secondFileName
     *
     * @return Portfolio
     */
    public function setSecondFileName($secondFileName)
    {
        $this->secondFileName = $secondFileName;

        return $this;
    }

    /**
     * Get secondFileName
     *
     * @return string
     */
    public function getSecondFileName()
    {
        return $this->secondFileName;
    }

    /**
     * Set thirdFileName
     *
     * @param string $thirdFileName
     *
     * @return Portfolio
     */
    public function setThirdFileName($thirdFileName)
    {
        $this->thirdFileName = $thirdFileName;

        return $this;
    }

    /**
     * Get thirdFileName
     *
     * @return string
     */
    public function getThirdFileName()
    {
        return $this->thirdFileName;
    }
    /**
     * @var integer
     */
    private $year;


    /**
     * Set year
     *
     * @param integer $year
     *
     * @return Portfolio
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }
}
