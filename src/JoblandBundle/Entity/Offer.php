<?php

namespace JoblandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offer
 *
 * @ORM\Table(name="offer")
 * @ORM\Entity
 */
class Offer
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=128, nullable=false)
     */
    private $city;


    /**
     * @var integer
     *
     * @ORM\Column(name="employer_id", type="integer", nullable=false)
     */
    private $employerId;

    /**
     * @var string
     *
     * @ORM\Column(name="country_id", type="string", length=32, nullable=false)
     */
    private $countryId;

    /**
     * @var string
     *
     * @ORM\Column(name="employer", type="text", nullable=false)
     */
    private $employer;

    /**
     * @var string
     *
     * @ORM\Column(name="job_description", type="text", nullable=false)
     */
    private $jobDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="requirements", type="text", nullable=false)
     */
    private $requirements;

    /**
     * @var string
     *
     * @ORM\Column(name="apply_offer", type="text", nullable=false)
     */
    private $applyOffer;

    /**
     * @var string
     *
     * @ORM\Column(name="apply_adress", type="string", length=64, nullable=false)
     */
    private $applyAdress;

    /**
     * @var integer
     *
     * @ORM\Column(name="offer_status", type="integer", nullable=false)
     */
    private $offerStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="offer_categor_id", type="integer", nullable=false)
     */
    private $offerCategorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set name
     *
     * @param string $name
     *
     * @return Offer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
     /**
     * Set city
     *
     * @param string $city
     *
     * @return Employer
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }


    /**
     * Set employerId
     *
     * @param integer $employerId
     *
     * @return Offer
     */
    public function setEmployerId($employerId)
    {
        $this->employerId = $employerId;

        return $this;
    }

    /**
     * Get employerId
     *
     * @return integer
     */
    public function getEmployerId()
    {
        return $this->employerId;
    }

    /**
     * Set countryId
     *
     * @param string $countryId
     *
     * @return Offer
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return string
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set employer
     *
     * @param string $employer
     *
     * @return Offer
     */
    public function setEmployer($employer)
    {
        $this->employer = $employer;

        return $this;
    }

    /**
     * Get employer
     *
     * @return string
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Set jobDescription
     *
     * @param string $jobDescription
     *
     * @return Offer
     */
    public function setJobDescription($jobDescription)
    {
        $this->jobDescription = $jobDescription;

        return $this;
    }

    /**
     * Get jobDescription
     *
     * @return string
     */
    public function getJobDescription()
    {
        return $this->jobDescription;
    }

    /**
     * Set requirements
     *
     * @param string $requirements
     *
     * @return Offer
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * Get requirements
     *
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * Set applyOffer
     *
     * @param string $applyOffer
     *
     * @return Offer
     */
    public function setApplyOffer($applyOffer)
    {
        $this->applyOffer = $applyOffer;

        return $this;
    }

    /**
     * Get applyOffer
     *
     * @return string
     */
    public function getApplyOffer()
    {
        return $this->applyOffer;
    }

    /**
     * Set applyAdress
     *
     * @param string $applyAdress
     *
     * @return Offer
     */
    public function setApplyAdress($applyAdress)
    {
        $this->applyAdress = $applyAdress;

        return $this;
    }

    /**
     * Get applyAdress
     *
     * @return string
     */
    public function getApplyAdress()
    {
        return $this->applyAdress;
    }

    /**
     * Set offerStatus
     *
     * @param integer $offerStatus
     *
     * @return Offer
     */
    public function setOfferStatus($offerStatus)
    {
        $this->offerStatus = $offerStatus;

        return $this;
    }

    /**
     * Get offerStatus
     *
     * @return integer
     */
    public function getOfferStatus()
    {
        return $this->offerStatus;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return Offer
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set offerCategorId
     *
     * @param integer $offerCategorId
     *
     * @return Offer
     */
    public function setOfferCategorId($offerCategorId)
    {
        $this->offerCategorId = $offerCategorId;

        return $this;
    }

    /**
     * Get offerCategorId
     *
     * @return integer
     */
    public function getOfferCategorId()
    {
        return $this->offerCategorId;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var integer
     */
    private $userId;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Offer
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
