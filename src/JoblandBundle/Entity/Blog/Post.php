<?php

namespace JoblandBundle\Entity\Blog;

/**
 * Post
 */
class Post
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

