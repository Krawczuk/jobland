<?php
namespace JoblandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="page")
 * @ORM\HasLifecycleCallbacks
 */
class Page
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="pages")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $userId;

	/**
	 * @ORM\Column(type="string", length=32)
	 */
	protected $language;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $nameMenu;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $slug;

	/**
	 * @ORM\Column(type="string", length=1024)
	 */
	protected $megaSlug;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $content;

	/**
	 * @ORM\Column(type="integer", length=11, options={"default":0})
	 */
	protected $parent = 0;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $descendants;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $children;

	/**
	 * @ORM\Column(type="boolean", nullable=true)
	 */
	protected $isPublished;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $path;

	/**
	 * @ORM\Column(type="integer", length=11, nullable=true)
	 */
	protected $kind;

	/**
	 * @ORM\Column(type="boolean", options={"default":0})
	 */
	protected $isDeleted = 0;

	/**
	 * @ORM\Column(type="integer", length=11, nullable=true)
	 */
	protected $position;

	/**
	 * @ORM\Column(type="boolean", options={"default":0})
	 */
	protected $isFeatured;

	/**
	 * @ORM\Column(type="string", length=1024, nullable=true)
	 */
	protected $externalUrl;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $options;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $createdAt;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $modifiedAt;

	/**
	 * @ORM\OneToOne(targetEntity="User",  mappedBy="homePageId")
	 */
	protected $homePageId;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set userId
	 *
	 * @param integer $userId
	 *
	 * @return Page
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;

		return $this;
	}

	/**
	 * Get userId
	 *
	 * @return integer
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * Set language
	 *
	 * @param string $language
	 *
	 * @return Page
	 */
	public function setLanguage($language)
	{
		$this->language = $language;

		return $this;
	}

	/**
	 * Get language
	 *
	 * @return string
	 */
	public function getLanguage()
	{
		return $this->language;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Page
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set nameMenu
	 *
	 * @param string $nameMenu
	 *
	 * @return Page
	 */
	public function setNameMenu($nameMenu)
	{
		$this->nameMenu = $nameMenu;

		return $this;
	}

	/**
	 * Get nameMenu
	 *
	 * @return string
	 */
	public function getNameMenu()
	{
		return $this->nameMenu;
	}

	/**
	 * Set slug
	 *
	 * @param string $slug
	 *
	 * @return Page
	 */
	public function setSlug($slug)
	{
		$this->slug = \JoblandBundle\Libs\Utils::slugify($slug);

		return $this;
	}

	/**
	 * Get slug
	 *
	 * @return string
	 */
	public function getSlug()
	{
		return $this->slug;
	}

	/**
	 * Set megaSlug
	 *
	 * @param string $megaSlug
	 *
	 * @return Page
	 */
	public function setMegaSlug($megaSlug)
	{
		$this->megaSlug = $megaSlug;

		return $this;
	}

	/**
	 * Get megaSlug
	 *
	 * @return string
	 */
	public function getMegaSlug()
	{
		
		
		return $this->megaSlug;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 *
	 * @return Page
	 */
	public function setContent($content)
	{
		$this->content = $content;

		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * Set parent
	 *
	 * @param integer $parent
	 *
	 * @return Page
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;

		return $this;
	}

	/**
	 * Get parent
	 *
	 * @return integer
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * Set descendants
	 *
	 * @param string $descendants
	 *
	 * @return Page
	 */
	public function setDescendants($descendants)
	{
		$this->descendants = $descendants;

		return $this;
	}

	/**
	 * Get descendants
	 *
	 * @return string
	 */
	public function getDescendants()
	{
		return $this->descendants;
	}

	/**
	 * Set children
	 *
	 * @param string $children
	 *
	 * @return Page
	 */
	public function setChildren($children)
	{
		$this->children = $children;

		return $this;
	}

	/**
	 * Get children
	 *
	 * @return string
	 */
	public function getChildren()
	{
		return $this->children;
	}

	/**
	 * Set isPublished
	 *
	 * @param boolean $isPublished
	 *
	 * @return Page
	 */
	public function setIsPublished($isPublished)
	{
		$this->isPublished = $isPublished;

		return $this;
	}

	/**
	 * Get isPublished
	 *
	 * @return boolean
	 */
	public function getIsPublished()
	{
		return $this->isPublished;
	}

	/**
	 * Set path
	 *
	 * @param string $path
	 *
	 * @return Page
	 */
	public function setPath($path)
	{
		$this->path = $path;

		return $this;
	}

	/**
	 * Get path
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Set kind
	 *
	 * @param boolean $kind
	 *
	 * @return Page
	 */
	public function setKind($kind)
	{
		$this->kind = $kind;

		return $this;
	}

	/**
	 * Get kind
	 *
	 * @return boolean
	 */
	public function getKind()
	{
		return $this->kind;
	}

	/**
	 * Set isDeleted
	 *
	 * @param boolean $isDeleted
	 *
	 * @return Page
	 */
	public function setIsDeleted($isDeleted)
	{
		$this->isDeleted = $isDeleted;

		return $this;
	}

	/**
	 * Get isDeleted
	 *
	 * @return boolean
	 */
	public function getIsDeleted()
	{
		return $this->isDeleted;
	}

	/**
	 * Set position
	 *
	 * @param integer $position
	 *
	 * @return Page
	 */
	public function setPosition($position)
	{
		$this->position = $position;

		return $this;
	}

	/**
	 * Get position
	 *
	 * @return integer
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * Set isFeatured
	 *
	 * @param boolean $isFeatured
	 *
	 * @return Page
	 */
	public function setIsFeatured($isFeatured)
	{
		$this->isFeatured = $isFeatured;

		return $this;
	}

	/**
	 * Get isFeatured
	 *
	 * @return boolean
	 */
	public function getIsFeatured()
	{
		return $this->isFeatured;
	}

	/**
	 * Set externalUrl
	 *
	 * @param string $externalUrl
	 *
	 * @return Page
	 */
	public function setExternalUrl($externalUrl)
	{
		$this->externalUrl = $externalUrl;

		return $this;
	}

	/**
	 * Get externalUrl
	 *
	 * @return string
	 */
	public function getExternalUrl()
	{
		return $this->externalUrl;
	}

	/**
	 * Set options
	 *
	 * @param string $options
	 *
	 * @return Page
	 */
	public function setOptions($options)
	{
		$this->options = $options;

		return $this;
	}

	/**
	 * Get options
	 *
	 * @return string
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return Page
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set modifiedAt
	 *
	 * @param \DateTime $modifiedAt
	 *
	 * @return Page
	 */
	public function setModifiedAt($modifiedAt)
	{
		$this->modifiedAt = $modifiedAt;

		return $this;
	}

	/**
	 * Get modifiedAt
	 *
	 * @return \DateTime
	 */
	public function getModifiedAt()
	{
		return $this->modifiedAt;
	}

	/**
	 * Transform to string
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string) $this->getId();
	}
	 
	public function __construct()
	{
		$this->createdAt = new \DateTime();
		//$this->userId = 1;
		
		$this->language = 'PL';
		
		
	}
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function preSave()
	{
		if($this->getSlug() == NULL) {
			$this->setSlug($this->getName());
		}

		if ($this->getParent() == NULL) {
			$this->setMegaSlug($this->getSlug());
			$this->setParent(0);
		} else {
			if(gettype($this->getParent()) == 'object') {
				$this->setMegaSlug($this->getParent()->getMegaSlug()."/".$this->getSlug());
			}
		}
		if ($this->getKind() == NULL) {
			$this->setKind(0);
		}
	}

	public static function buildDescendants($id=0,$pages=array())
	{
		$descendants=array();
		$descendants[]=$id;

		foreach($pages as $page)
		{
			$path=explode(" ",$page->getPath());
			if(in_array($id,$path)) { $descendants=array_merge($descendants,explode(" ",$page->getChildren())); }
		}
		$descendants=array_unique($descendants);

		return $descendants;
	}

	public static function buildPath($id=0, $path=array(), $pages = array())
	{
		if($id) {
			$page=$pages[$id];
			$path[]=$page->getId();

			if(gettype($page->getParent()) == 'object') {
				$parent = $page->getParent()->getId();
			} else {
				$parent = $page->getParent();
			}
			if($parent != 0){
				return self::buildPath($parent, $path,$pages);
			}
		}
		return $path;
	}


	public static function buildMegaslug($id=0, $path=array(), $pages = array())
	{
		$path = array_reverse($path);

		$slug=array();
		foreach($path as $p){
			$slug[] = $pages[$p]->getSlug();
		}

		if($slug){
			return implode("/",$slug);
		}
		else{
			return $pages[$id]->getSlug();
		}
	}

	public static function buildChildren($id=0, $pages=array())
	{
		$children=array();
		foreach($pages as $page){

			if(gettype($page->getParent()) == 'object') {
				$parent = $page->getParent()->getId();
			} else {
				$parent = $page->getParent();
			}

			if($parent==$id){
				$children[]=$page->getId();
			}
		}
		return $children;
	}
	 
	public static function buildtree($branch, &$flattenTree=array())
	{
		if(!empty($branch)){
			foreach($branch as $k=>$v){
				 
				$flattenTree[]=$k;
				if(!empty($v)){
					self::buildtree($v,$flattenTree);
				}
			}
		}
		return $flattenTree;
	}

	public static function buildOptions($array)
	{

		$options = array();
		if(isset($array['option'])){
			foreach($array['option'] as $key=>$value){
				$options[$key]=$value;
			}
		}

		$options = base64_encode(serialize($options));
		return $options;
	}

	public function getOption($name)
	{
		$options = unserialize(base64_decode($this->getOptions()));
		if(isset($options[$name])){
			return $options[$name];
		} else {
			return null;
		}
	}

	public function setOption($name,$value)
	{
		$options = unserialize(base64_decode($this->getOptions()));
		$options[$name]=$value;
		$this->options=base64_encode(serialize($options));
	}

	public static function allTemplates()
	{
		$templatePath = '../src/JoblandBundle/Resources/views/Templates';
		$templates = array();
		$directories = glob($templatePath . '/*' , GLOB_ONLYDIR);

		foreach ($directories as $value) {
			$templates[basename($value)] = basename($value);
		}

		return $templates;
	}

	/**
	 * Set homePageId
	 *
	 * @param \JoblandBundle\Entity\User $homePageId
	 *
	 * @return Page
	 */
	public function setHomePageId(\JoblandBundle\Entity\User $homePageId = null)
	{
		$this->homePageId = $homePageId;

		return $this;
	}

	/**
	 * Get homePageId
	 *
	 * @return \JoblandBundle\Entity\User
	 */
	public function getHomePageId()
	{
		return $this->homePageId;
	}
}
