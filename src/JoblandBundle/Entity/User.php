<?php

namespace JoblandBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Util\SecureRandom;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
	/**
	 * @var string
	 */
	protected $company;

	/**
	 * @var integer
	 */
	protected $phoneNumber;

	/**
	 * @var integer
	 */
	protected $nip;

	/**
	 * @var string
	 */
	protected $contactPerson;

	/**
	 * @Assert\File(maxSize="2048k")
	 * @Assert\Image(mimeTypesMessage="Please upload a valid image.")
	 */
	protected $profilePictureFile;
	
	// for temporary storage
	private $tempProfilePicturePath;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $profilePicturePath;

	/**
	 * @var string
	 */
	protected $url;

	/**
	 * @var string
	 */
	protected $address;

	/**
	 * @var string
	 */
	protected $postalCode;
	
	/**
	 * @ORM\OneToMany(targetEntity="Page", mappedBy="userId")
	 */
	protected $pages;
	

	/**
	 * @var string
	 */
	protected $city;

	/**
	 * @var string
	 */
	protected $countryId;

	/**
	 * @var integer
	 */
	protected $agreement;

	/**
	 * @var integer
	 */
	protected $rules;

	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $id;

	
	
	
	public function setEmail($email)
	{
		$email = is_null($email) ? '' : $email;
		parent::setEmail($email);
		$this->setUsername($email);
	}

	public function __construct()
	{
		$this->pages = new \Doctrine\Common\Collections\ArrayCollection();
		parent::__construct();
		$this->addRole("ROLE_EMPLOYER");
	}

	/**
	 * Set company
	 *
	 * @param string $company
	 *
	 * @return User
	 */
	public function setCompany($company)
	{
		$this->company = $company;

		return $this;
	}

	/**
	 * Get company
	 *
	 * @return string
	 */
	public function getCompany()
	{
		return $this->company;
	}

	/**
	 * Set phoneNumber
	 *
	 * @param integer $phoneNumber
	 *
	 * @return User
	 */
	public function setPhoneNumber($phoneNumber)
	{
		$this->phoneNumber = $phoneNumber;

		return $this;
	}

	/**
	 * Get phoneNumber
	 *
	 * @return integer
	 */
	public function getPhoneNumber()
	{
		return $this->phoneNumber;
	}

	/**
	 * Set nip
	 *
	 * @param integer $nip
	 *
	 * @return User
	 */
	public function setNip($nip)
	{
		$this->nip = $nip;

		return $this;
	}

	/**
	 * Get nip
	 *
	 * @return integer
	 */
	public function getNip()
	{
		return $this->nip;
	}

	/**
	 * Set contactPerson
	 *
	 * @param string $contactPerson
	 *
	 * @return User
	 */
	public function setContactPerson($contactPerson)
	{
		$this->contactPerson = $contactPerson;

		return $this;
	}

	/**
	 * Get contactPerson
	 *
	 * @return string
	 */
	public function getContactPerson()
	{
		return $this->contactPerson;
	}

	/**
	 * Set url
	 *
	 * @param string $url
	 *
	 * @return User
	 */
	public function setUrl($url)
	{
			
		$this->url = $url;

		return $this;
	}

	/**
	 * Get url
	 *
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Set address
	 *
	 * @param string $address
	 *
	 * @return User
	 */
	public function setAddress($address)
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * Get address
	 *
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Set postalCode
	 *
	 * @param string $postalCode
	 *
	 * @return User
	 */
	public function setPostalCode($postalCode)
	{
		$this->postalCode = $postalCode;

		return $this;
	}

	/**
	 * Get postalCode
	 *
	 * @return string
	 */
	public function getPostalCode()
	{
		return $this->postalCode;
	}

	/**
	 * Set city
	 *
	 * @param string $city
	 *
	 * @return User
	 */
	public function setCity($city)
	{
		$this->city = $city;

		return $this;
	}

	/**
	 * Get city
	 *
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Set countryId
	 *
	 * @param string $countryId
	 *
	 * @return User
	 */
	public function setCountryId($countryId)
	{
		$this->countryId = $countryId;

		return $this;
	}

	/**
	 * Get countryId
	 *
	 * @return string
	 */
	public function getCountryId()
	{
		return $this->countryId;
	}

	/**
	 * Set agreement
	 *
	 * @param integer $agreement
	 *
	 * @return User
	 */
	public function setAgreement($agreement)
	{
		$this->agreement = $agreement;

		return $this;
	}

	/**
	 * Get agreement
	 *
	 * @return integer
	 */
	public function getAgreement()
	{
		return $this->agreement;
	}

	/**
	 * Set rules
	 *
	 * @param integer $rules
	 *
	 * @return User
	 */
	public function setRules($rules)
	{
		$this->rules = $rules;

		return $this;
	}

	/**
	 * Get rules
	 *
	 * @return integer
	 */
	public function getRules()
	{
		return $this->rules;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
    /**
     * @var string
     */
    protected $domain;

    /**
     * @var string
     */
    protected $subdomain;
    
   
    


    /**
     * Set domain
     *
     * @param string $domain
     *
     * @return User
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set subdomain
     *
     * @param string $subdomain
     *
     * @return User
     */
    public function setSubdomain($subdomain)
    {
        $this->subdomain = $subdomain;

        return $this;
    }

    /**
     * Get subdomain
     *
     * @return string
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }
    /**
     * Add page
     *
     * @param \JoblandBundle\Entity\Page $page
     *
     * @return User
     */
    public function addPage(\JoblandBundle\Entity\Page $page)
    {
    	$this->pages[] = $page;
    
    	return $this;
    }
    
    /**
     * Remove page
     *
     * @param \JoblandBundle\Entity\Page $page
     */
    public function removePage(\JoblandBundle\Entity\Page $page)
    {
    	$this->pages->removeElement($page);
    }
    
    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
    	return $this->pages;
    }
    

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Get expired
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean
     */
    public function getCredentialsExpired()
    {
        return $this->credentialsExpired;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return \DateTime
     */
    public function getCredentialsExpireAt()
    {
        return $this->credentialsExpireAt;
    }
   
 

    
    
    /**
     * Sets the file used for profile picture uploads
     *
     * @param UploadedFile $file
     * @return object
     */
    public function setProfilePictureFile(UploadedFile $file = null) {
    	// set the value of the holder
    	
    	$this->profilePictureFile = $file;
    	
    
    	
    	// check if we have an old image path
    	if (isset($this->profilePicturePath)) {
    		// store the old name to delete after the update
    		$this->tempProfilePicturePath = $this->profilePicturePath;
    		$this->profilePicturePath = null;
    	} else {
    		$this->profilePicturePath = '';
    	}
    
    	return $this;
    }
    
    /**
     * Get the file used for profile picture uploads
     *
     * @return UploadedFile
     */
    public function getProfilePictureFile() {
    
    	return $this->profilePictureFile;
    }
    
    /**
     * Set profilePicturePath
     *
     * @param string $profilePicturePath
     * @return User
     */
    public function setProfilePicturePath($profilePicturePath)
    {
    	
    	$this->profilePicturePath = $profilePicturePath;
    
    	return $this;
    }
    
    /**
     * Get profilePicturePath
     *
     * @return string
     */
    public function getProfilePicturePath()
    {
    	return $this->profilePicturePath;
    }
    
    /**
     * Get the absolute path of the profilePicturePath
     */
    public function getProfilePictureAbsolutePath() {
    	return null === $this->profilePicturePath
    	? null
    	: $this->getUploadRootDir().'/'.$this->profilePicturePath;
    }
    
    /**
     * Get root directory for file uploads
     *
     * @return string
     */
    protected function getUploadRootDir($type='profilePicture') {
    	// the absolute directory path where uploaded
    	// documents should be saved
    	return __DIR__.'//../../../../../../web/'.$this->getUploadDir($type);
    }
    
    /**
     * Specifies where in the /web directory profile pic uploads are stored
     *
     * @return string
     */
    protected function getUploadDir($type='profilePicture') {
    	// the type param is to change these methods at a later date for more file uploads
    	// get rid of the __DIR__ so it doesn't screw up
    	// when displaying uploaded doc/image in the view.
    	return 'uploads/user/profilepic';
    }
    
    /**
     * Get the web path for the user
     *
     * @return string
     */
    public function getWebProfilePicturePath() {
    
    	return '/'.$this->getUploadDir().'/'.$this->getProfilePicturePath();
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUploadProfilePicture() {
    	if (null !== $this->getProfilePictureFile()) {
    		// a file was uploaded
    		// generate a unique filename
    		$filename = $this->generateRandomProfilePictureFilename();
    		echo $this->setProfilePicturePath($filename.'.'.$this->getProfilePictureFile()->guessExtension());
    		
    		
    	}
    }
    
    /**
     * Generates a 32 char long random filename
     *
     * @return string
     */
    public function generateRandomProfilePictureFilename() {
    	$count = 0;
    	do {
    		$generator = new SecureRandom();
    		$random = $generator->nextBytes(16);
    		$randomString = bin2hex($random);
    		$count++;
    	}
    	while(file_exists($this->getUploadRootDir().'/'.$randomString.'.'.$this->getProfilePictureFile()->guessExtension()) && $count < 50);
    
    	return $randomString;
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     *
     * Upload the profile picture
     *
     * @return mixed
     */
    public function uploadProfilePicture() {
    	// check there is a profile pic to upload
    	if ($this->getProfilePictureFile() === null) {
    		return;
    	}
    	// if there is an error when moving the file, an exception will
    	// be automatically thrown by move(). This will properly prevent
    	// the entity from being persisted to the database on error
    	$this->getProfilePictureFile()->move($this->getUploadRootDir(), $this->getProfilePicturePath());
    	
    
    	// check if we have an old image
    	if (isset($this->tempProfilePicturePath) && file_exists($this->getUploadRootDir().'/'.$this->tempProfilePicturePath)) {
    		// delete the old image
    		unlink($this->getUploadRootDir().'/'.$this->tempProfilePicturePath);
    		// clear the temp image path
    		$this->tempProfilePicturePath = null;
    	}
    	
    	$this->profilePictureFile = null;
    	
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeProfilePictureFile()
    {
    	if ($file = $this->getProfilePictureAbsolutePath() && file_exists($this->getProfilePictureAbsolutePath())) {
    		unlink($file);
    	}
    }
}
