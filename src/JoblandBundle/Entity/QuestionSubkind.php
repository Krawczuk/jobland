<?php

namespace JoblandBundle\Entity;

/**
 * QuestionSubkind
 */
class QuestionSubkind
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $questionKindId;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return QuestionSubkind
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set questionKindId
     *
     * @param integer $questionKindId
     *
     * @return QuestionSubkind
     */
    public function setQuestionKindId($questionKindId)
    {
        $this->questionKindId = $questionKindId;

        return $this;
    }

    /**
     * Get questionKindId
     *
     * @return integer
     */
    public function getQuestionKindId()
    {
        return $this->questionKindId;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
