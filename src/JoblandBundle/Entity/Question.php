<?php

namespace JoblandBundle\Entity;

/**
 * Question
 */
class Question
{
    /**
     * @var string
     */
    private $question;

    /**
     * @var string
     */
    private $content;

    /**
     * @var integer
     */
    private $questionSubkindId;

    /**
     * @var integer
     */
    private $questionKindId;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set question
     *
     * @param string $question
     *
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Question
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set questionSubkindId
     *
     * @param integer $questionSubkindId
     *
     * @return Question
     */
    public function setQuestionSubkindId($questionSubkindId)
    {
        $this->questionSubkindId = $questionSubkindId;

        return $this;
    }

    /**
     * Get questionSubkindId
     *
     * @return integer
     */
    public function getQuestionSubkindId()
    {
        return $this->questionSubkindId;
    }

    /**
     * Set questionKindId
     *
     * @param integer $questionKindId
     *
     * @return Question
     */
    public function setQuestionKindId($questionKindId)
    {
        $this->questionKindId = $questionKindId;

        return $this;
    }

    /**
     * Get questionKindId
     *
     * @return integer
     */
    public function getQuestionKindId()
    {
        return $this->questionKindId;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
