<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\CreatePageType;
use AppBundle\Form\Type\SetTemplateType;
use Symfony\Component\HttpFoundation\Session\Session;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\PropertyPromoted;
use AppBundle\Libs\Utils;

class PageController extends Controller
{
    /**
     * @Route("/", name="backendHomepage")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Admin:index.html.twig');
    }
    
    /**
     * @Route("/admin/info-page", name="pageList")
     */
    public function pageListOnAction()
    {
        $Repo = $this->getDoctrine()->getRepository('AppBundle:Page');
        $pages = $Repo->findBy(array('isDeleted' => 0, 'userId' => $this->getUser()), array('position' => 'ASC'));
        
        $node = '';
        $info = '';        
        foreach($pages as $page){
            $info[$page->getId()]=$page;
            $path = array_reverse(explode(' ',$page->getPath()),true);

            $code='if(!isset($node';
            foreach($path as $key=>$value){
                $code .= '[' . $value . ']';
            }
            $code .= ')) $node';
            foreach($path as $key=>$value){
                $code .= '[' . $value . ']';
            }
            $code .= '=array();';

            eval($code);
            
            $tree[0]=$node;
        }
        
        return $this->render('JoblandBundle:admin/pageList.html.twig', array(
            'flattenTree' => Page::buildtree($node),
            'pages'=>$info
        ));
    }

    /**
     * @Route("/oferty-promowane", name="propertyPromoted")
     */
    public function propertyPromotedListAction(Request $request) 
    {
        
        $Repo = $this->getDoctrine()->getRepository('AppBundle:Property');
        $property = $Repo->findBy(array('user' => $this->getUser()));
        
        $Repo = $this->getDoctrine()->getRepository('AppBundle:PropertyPromoted');
        $propertyPromoted = $Repo->findOneBy(array('userId' => $this->getUser()));
        if($propertyPromoted) {
            $propertyPromoted = json_decode($propertyPromoted->getValue(), true);
        } else {
            $propertyPromoted = array();
        }
        
        return $this->render('AppBundle:Admin:promotedList.html.twig', array(
                'property'=>$property,
                'propertyPromoted'=>$propertyPromoted
        ));

    }
    
    /**
     * @Route("/oferty-promowane-zapisz", name="propertyPromotedSave")
     */
    public function propertyPromotedSaveAction(Request $request)
    {
        if($request->isMethod('POST')) {
            $postData = $request->request->get('promoted');
            $promoted = array();
            foreach($postData as $post) {
                $promoted[] = $post;
            }
            $promoted = json_encode($promoted);
            
            $Repo = $this->getDoctrine()->getRepository('AppBundle:PropertyPromoted');
            $propertyPromoted = $Repo->findOneBy(array('userId' => $this->getUser()));
            
            if(!$propertyPromoted) {
                $propertyPromoted = new PropertyPromoted;
                $propertyPromoted->setUserId($this->getUser());
            }
            
            $em = $this->getDoctrine()->getManager();
            $propertyPromoted->setValue($promoted);
            $em->persist($propertyPromoted);
            $em->flush();
            
            
        }
        die();
    }
    
    
    /**
     * @Route("/strony-informacyjne/dodaj", name="addPage")
     */
    public function addPageAction(Request $request)
    {
        //var_dump($this->getUser());
       	$page = new Page;
    	 
        $form = $this->createForm(CreatePageType::class, $page);
    	
    	$form->handleRequest($request);
    	
    	if ($form->isSubmitted() && $form->isValid()) {   
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            
            $page->setOptions(Page::buildOptions($request->request->get('create_page')));
            
            $slug = $this->checkSlug($page);
            $page->setUserId($this->getUser());
            $page->setSlug($slug);
            $em->persist($page);
            $em->flush();
            
            $this->rebuildPagesTree();
            
            return $this->redirectToRoute('pageList');
    	}
        
        return $this->render('AppBundle:Admin:addPage.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/strony-informacyjne/edytuj/{id}", name="editPage")
     */
    public function editPageAction(Request $request, $id)
    {
        $Repo = $this->getDoctrine()->getRepository('AppBundle:Page');
        $page = $Repo->find($id);
        
        if($page == NULL OR $page->getUserId() != $this->getUser()) {
            throw $this->createNotFoundException('Wystąpił błąd!');
        }
        
        $form = $this->createForm(CreatePageType::class, $page);
        
        if($request->isMethod('POST')) {
            $session = new Session();
            $form->handleRequest($request);
            
            if($form->isValid()) { 
                $page->setModifiedAt(new \DateTime());
                
                $page->setOptions(Page::buildOptions($request->request->get('create_page')));
                
                // Check if slug exist
                $slug = $this->checkSlug($page);
                $page->setSlug($slug);
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($page);
                $em->flush();
                
                // set path / childrens / mega-slug
                $this->rebuildPagesTree();
                
                $session->getFlashBag()->add('alert alert-success', 'Zaktualizowano rekord.');
                
                return $this->redirect($this->generateUrl('pageList'));
            } else {
                $session->getFlashBag()->add('alert alert-danger', 'Popraw błędy formularza!');
            }
        }
        
        return $this->render('AppBundle:Admin:addPage.html.twig', array(
            'form' => $form->createView(),
            'register' => $page
        ));
    }
    
    /**
     * @Route("/strony-informacyjne/load-custom-field", name="pageCustomField")
     */
    public function pageCustomField(Request $request)
    {
        if ( $request->isXmlHttpRequest() ) {
            $val = $request->get("id");
            $pageId = $request->get("pageId");
            if ($pageId != 0) {
                $Repo = $this->getDoctrine()->getRepository('AppBundle:Page');
                $page = $Repo->find($pageId);
            }
            
            if ($val != '') {
                return $this->render('AppBundle:Admin:template'.$val.'Form.html.twig', array(
                    'register' => isset($page)? $page : ''
                ));
            }
        }
        return new Response;
    }
    
    /**
     * @Route("/strony-informacyjne/usun/{id}", name="deletePage")
     */
    public function deletePageAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AppBundle:Page')->find($id);
        
        if($page == NULL OR $page->getUserId() != $this->getUser()) {
            throw $this->createNotFoundException('Wystąpił błąd!');
        }
        $session = new Session();

        $page->setIsDeleted(1);
        $em->flush();
        
        $session->getFlashBag()->add('alert alert-success', 'Poprawnie usunięto rekord.');
        
        return $this->redirect($this->generateUrl('pageList'));
    }
    
    /**
     * @Route("/strony-informacyjne/position-update", name="pageListPositionUpdate")
     */
    public function executePositionUpdateAction(Request $request)
    {
        if ( $request->isXmlHttpRequest() ) {        
    
            $data = $request->get("sortedList");
           
            $parents=array();
            foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($data), \RecursiveIteratorIterator::SELF_FIRST) as $key => $value) {
                if(isset($value['children'])){
                    foreach($value['children'] as $k=>$v){
                        $parents[$v['id']]=$value['id'];
                    }
                }
            }
            
            $i=0;
            $positions=array();
            foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($data), \RecursiveIteratorIterator::SELF_FIRST) as $key => $value) {
                if($key==="id"){
                    $i++;
                    $positions[$value]=$i;
                }
            }

            foreach($positions as $id=>$position){
                if(!isset($parents[$id])){
                    $parents[$id]=0;
                }
            }

            $repo = $this->getDoctrine()->getRepository('AppBundle:Page');
            
            foreach($parents as $id=>$parent) {
                $curPage = $repo->find($id);
                $curPage->setParent($parent);
                $curPage->setPosition($positions[$id]);
                  
                $em = $this->getDoctrine()->getManager();
                $em->persist($curPage);
                $em->flush();
            }
            
            $this->rebuildPagesTree();
        }
        return new Response();
    }
    
    /**
     * @Route("/funkcje", name="backendFunctions")
     */
    public function functionAction(Request $request)
    {        
        $user = $this->getUser();
        $repo = $this->getDoctrine()->getRepository('AppBundle:Config');
        $availableFunctions = $repo->findOneBy(array('name' => 'functionsAvailable', 'userId' => $user));
        $availableFunctions = json_decode($availableFunctions->getValue(), true);
        
        $enabledFunctions = $repo->findOneBy(array('name' => 'functionsEnabled', 'userId' => $user));
        $enabledFunctions = json_decode($enabledFunctions->getValue(), true);
        
        $functions = $this->get('app.powernet.utils')->getAllFunctions();
        return $this->render('AppBundle:Admin:functionsList.html.twig', array(
                'functions' => $functions,
                'enabledFunctions' => $enabledFunctions,
                'availableFunctions' => $availableFunctions
        ));
    }
    
    /**
     * @Route("/funkcje/zapisz", name="backendSaveFunctions")
     */
    public function functionSaveAction(Request $request)
    {
        if($request) {
            $data = $request->request->get('functions');
            
            $repo = $this->getDoctrine()->getRepository('AppBundle:Config');
            $functions = $repo->findOneBy(array('name' => 'functionsEnabled', 'userId' => $this->getUser()->getId()));
            $functionsEnabled = json_decode($functions->getValue(),true);
            
            foreach($functionsEnabled as $key => $val) {
                if(array_key_exists($key,$data)) {
                    $functionsEnabled[$key] = 1;
                } else {
                    $functionsEnabled[$key] = 0;
                }
            }
            $functions->setValue(json_encode($functionsEnabled));
        
            $em = $this->getDoctrine()->getManager();
            $em->persist($functions);
            $em->flush();
        }
        return new Response;
    }
    
    /**
     * @Route("/ustawienia", name="config")
     */
    public function configAction(Request $request)
    {
        $user = $this->getUser();
        
        $form = $this->createForm(SetTemplateType::class, $user);
    	$form->handleRequest($request);
    	
    	$repo = $this->getDoctrine()->getRepository('AppBundle:Config');
    	$options = $repo->findOneBy(array('name' => 'agentImages', 'userId' => $user->getId()));
    	$customCss = $repo->findOneBy(array('name' => 'customCss', 'userId' => $user->getId()));
    	
    	if ($form->isSubmitted() && $form->isValid()) {   
    	    
    	    if($request->request->get('agentImages')) { 
    	        $agentImages = 1;
    	    } else {    
    	        $agentImages = 0;
    	    }
    	    $css = $request->request->get('customCss');
    	    
    	    $options->setValue($agentImages);
    	    $customCss->setValue($css);
               
    	    $em = $this->getDoctrine()->getManager();
    	    $em->persist($options);
    	    $em->flush();
    	    
    	    $em = $this->getDoctrine()->getManager();
    	    $em->persist($customCss);
    	    $em->flush();
    	    
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            return $this->redirect($this->generateUrl('config'));
        }
        
        return $this->render('AppBundle:Admin:config.html.twig', array(
            'form' => $form->createView(),
            'customCss'  => $customCss->getValue(),
            'agentImage' => $options->getValue()
        ));
    }
    
    /**
     * @Route("/bloki", name="shortcodeList")
     */
    public function shortcodeAction()
    {
        $repo = $this->getDoctrine()->getRepository('AppBundle:Config');
        $shortcodes = $repo->findOneBy(array('name' => 'shortcode', 'userId' => $this->getUser()->getId()));
        
        $data = json_decode($shortcodes->getValue(), true);
        
        return $this->render('AppBundle:Admin:shortcodeList.html.twig', array(
            'shortcodes' => $data
        ));
    }
    
    /**
     * @Route("/bloki/zapisz", name="shortcodeUpdate")
     */
    public function shortcodeUpdateAction(Request $request)
    {
        if($request) {
            $data = $request->request->get('shortcode');
            
            $repo = $this->getDoctrine()->getRepository('AppBundle:Config');
            $shortcode = $repo->findOneBy(array('name' => 'shortcode', 'userId' => $this->getUser()->getId()));
            $shortcode->setValue(json_encode($data));
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($shortcode);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('shortcodeList'));
    }
    
    
    
    
    
    private function checkSlug($page) 
    {
        
        $repo = $this->getDoctrine()->getRepository('AppBundle:Page');
        $qb = $repo->createQueryBuilder('p');
        
        $qb->where('p.id != :identifier')
            ->andWhere('p.userId = :userid')
            ->setParameter('identifier', $page->getId())
            ->setParameter('userid', $page->getUserId());
        
        $pages = $qb->getQuery()->getResult();
        
        $page->setSlug($page->getName());
        $baseSlug = $page->getSlug();

        $i=1;
        foreach($pages as $val) {
            while($val->getSlug() == $page->getSlug()) {

                $page->setSlug($baseSlug."-".$i);
                $i++;
            }
        }
        return $page->getSlug();
    }
    
    public function rebuildPagesTree()
    {
        $conn = $this->get('database_connection');
        ini_set('max_execution_time', 300);

        $repo = $this->getDoctrine()->getRepository('AppBundle:Page');
        $pages = $repo->findBy(array('isDeleted' => 0, 'userId' => $this->getUser()), array('parent' => 'ASC', 'position' => 'ASC'));
        
        $tmp=$pages;
        $pages=array();

        if(!empty($tmp))
        {
            foreach($tmp as $_tmp) { $pages[$_tmp->getId()]=$_tmp; }
        }

        if(!empty($pages)) {
            $q="INSERT INTO `page` (id,path,children) VALUES ";
            $tmp=array();

            $i=0;
            foreach($pages as $key=>$page) {
                $children = Page::buildChildren($page->getId(),$pages);
                $children = array_filter($children);
                $children = array_unique($children);
                $page->setChildren(!empty($children)?implode(" ",$children):'');

                $path = Page::buildPath($page->getId(),array(),$pages);
                $path=array_filter($path);
                $path=array_unique($path);
                $page->setPath(!empty($path)?implode(" ",$path):'');

                $pages[$key]=$page;

                $tmp[]=" (".$page->getId().",'".trim(implode(" ",$path))."','".trim(implode(" ",$children))."')";
            }

            $q.=implode(",", $tmp);
            $q.=" ON DUPLICATE KEY UPDATE path=values(path), children=values(children)";

            // insert data
            if(!empty($tmp)) { $conn->prepare($q)->execute(); }

            $q="INSERT INTO `page` (id,mega_slug,descendants) VALUES ";
            $tmp=array();
            $i=0;

            foreach($pages as $key=>$page)
            {
                $megaslug= Page::buildMegaSlug($page->getId(),explode(" ",$page->getPath()),$pages);
                $page->setMegaSlug($megaslug);

                $descendants= Page::buldDescendants($page->getId(),$pages);
                $descendants=array_filter($descendants);
                $descendants=array_unique($descendants);
                $page->setDescendants(!empty($descendants)?trim(implode(" ",$descendants)):'');

                $pages[$key]=$page;

                $tmp[]=" (".$page->getId().",'".$megaslug."','".trim(implode(" ",$descendants))."')";
            }

            $q.=implode(",", $tmp);
            $q.=" ON DUPLICATE KEY UPDATE mega_slug=values(mega_slug), descendants=values(descendants)";

            // insert data
            if(!empty($tmp)) { $conn->prepare($q)->execute(); }
        }
    }
}
