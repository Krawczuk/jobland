<?php
namespace JoblandBundle\Controller;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Asset;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use JoblandBundle\Entity\Employer;
use JoblandBundle\Entity\Category;
use JoblandBundle\Entity\Country;
use JoblandBundle\Entity\Page;
use JoblandBundle\Form\Type\CreatePageType;
use JoblandBundle\Libs\Utils;
use JoblandBundle\Entity\News;
use JoblandBundle\Entity\Offer;
use JoblandBundle\Entity\Media;
use JoblandBundle\Entity\Portfolio;
use JoblandBundle\Entity\Partners;
use JoblandBundle\Entity\CvDatabase;
use JoblandBundle\Entity\Question;
use JoblandBundle\Entity\QuestionSubkind;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Security\Core\Security;
use JoblandBundle\Entity\Newsletter;
use Swift_Attachment;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Knp\Bundle\PaginatorBundle;
use Symfony\Component\HttpFoundation\Response;
use JoblandBundle\Entity\QuestionKind;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;




    
class BlogController extends Controller
{
	
	
	
	
	public function findByNot()
	{
		$qb = $this->createQueryBuilder('u');
		$qb->where('u.id != :identifier')
		->setParameter('identifier', 1);
	
		return $qb->getQuery()
		->getResult();
	}
	
	
// 	public function executeMenuTopAction() {


// 		$pages  = array(
// 			1 => array(
// 				'name' => 'O NAS',
// 				'url' => '/o-nas', 
// 				'child' => array(
// 					1 => array(
// 						'name' => 'Ogólne informacje',
// 						'url' => '/o-nas' 
// 					),
// 					2 => array(
// 						'name' => 'Media',
// 						'url' => '/media' 
// 					),
// 					3 => array(
// 						'name' => 'Portfolio i referencje',
// 						'url' => '/portfolio' 
// 					),

// 					4 => array(
// 						'name' => 'Partnerzy',
// 						'url' => '/partnerzy' 
// 					),
// 				)
// 			),
// 				2 => array(
// 				'name' => 'Dla pracodawców',
// 				'url' => '/jak-dzialamy', 
// 				'child' => array(
// 					1 => array(
// 						'name' => 'Jak działamy',
// 						'url' => '/jak-dzialamy' 
// 					),
// 					2 => array(
// 						'name' => 'Zamieszczanie ofert pracy',
// 						'url' => '/zamieszczanie-ofert' 
// 					),
// 					3 => array(
// 						'name' => 'Newsletter',
// 						'url' => '/newsletter' 
// 					),

// 					4 => array(
// 						'name' => 'Branża budownictwa budowlanego',
// 						'url' => '/branza' 
// 					),
// 				)
// 			),
// 				3 => array(
// 				'name' => 'Dla kandytatów',
// 				'url' => '/jak-dziala', 
// 				'child' => array(
// 					1 => array(
// 						'name' => 'Jak działa Jobland.pl',
// 						'url' => '/jak-dziala' 
// 					),
// 					2 => array(
// 						'name' => 'Oferty pracy',
// 						'url' => '/oferty-pracy' 
// 					),
// 					3 => array(
// 						'name' => 'FAQ dla wyjeżdzających',
// 						'url' => '/faq' 
// 					),

// 					4 => array(
// 						'name' => 'Porady i Wskazówki',
// 						'url' => '/porady-wskazowki' 
// 					),
// 				)
// 			),

// 				4 => array(
// 				'name' => 'Forum',
// 				'url' => '/forum',
				
// 				),	
// 				5 => array(
// 				'name' => 'Kontakt',
// 				'url' => '/kontakt',
				
// 				),	
// 		);
// 	return $this->render('JoblandBundle:Blog:menuTop.html.twig', array(
// 			'pages' => $pages
// 		));	
// 	}
	
	
	
	/**
     * @Route("/footer")
     *
     */
	public function footerAction() {
		
		}
		
		
		/**
		 * @Route("/", name="homepage")
		 *
		 */
		public function IndexAction(Request $request) {
			
			
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
			
		$rows = $Repo ->findAll();
		$pl = $Repo ->findByCountryId('PL');
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:News');
			
		$news = $Repo ->findAll();
		
		return $this->render('JoblandBundle:Blog:index.html.twig', array(
			
				'rows' => $rows,
				'news' => $news,
				'pl' => $pl
		));
		}
			
		
		/**
		 * @Route("/admin/admin-panel")
		 *
		 */
		public function AdminPanelAction() {
			return $this->render('JoblandBundle:Blog:admin/admin-panel.html.twig', array());
		}
	
		/**
		 * @Route("/admin/about-us", name="admin-about-us")
		 *
		 */
		public function AdminAboutUsAction(Request $request) {
			
			
			$media = new Media;
			
			$mediaform = $this->createFormBuilder($media)
			->add('title', TextType::class, array(
					'label' => 'Tytuł'))
			->add('date', TextType::class, array(
							'label' => 'Date'
							
							)
							
			)
			->add('content', TextAreaType::class, array(
									'label' => 'Treść',
									'attr' => array('class' => 'ckeditor')
			))
			->add('url', TextType::class, array(
											'label' => 'Adres url'))
			->add('dodaj', SubmitType::class)
							->getForm();
							$mediaform->handleRequest($request);
								
			
							if($mediaform->isValid()){
			
								
								
								$title = $mediaform['title']->getData();
								$content = $mediaform['content']->getData();
								$date = $mediaform['date']->getData();
								$url = $mediaform['url']->getData();
								
								$timestamp = strtotime($date);
								$date = date('Y-m-d',$timestamp );
								
							
							
								
								
								
							
								$status = 1;
									
								$media->setContent($content);
								$media->setTitle($title);
								$media->setStatus($status);
								$media->setDate($date);
								$media->setUrl($url);
			
								$em = $this->getDoctrine()->getManager();
								$em->persist($media);
								$em->flush();
			
								return $this->redirectToRoute('admin-about-us');
							}
			
			
			
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Media');
							$mediarows = $Repo ->findAll();
							
							$partners = new Partners;
								
							$partnersform = $this->createFormBuilder($partners)
							->add('name', TextType::class, array(
									'label' => 'Tytuł'))
									
											
							->add('text', TextAreaType::class, array(
													'label' => 'Treść',
													'attr' => array('class' => 'ckeditor')
											))
							->add('link', TextType::class, array(
													'label' => 'Link',
													
											))
							->add('logourl', FileType::class, array(
													'label' => 'Dodaj Logo'))
													->add('dodaj', SubmitType::class)
													->getForm();
							$partnersform->handleRequest($request);
							
														
									if($partnersform->isValid()){
															
										$savePath = $this->get('kernel')->getRootDir().'/../web/uploads/partners/';
										
										$formData = $partnersform->getData();
										
										$randVal = rand(1000,9999);
										
											
										$file = $partnersform->get('logourl')->getData();
										if(NULL !=$file){
											$newName = sprintf('img__%d.%s', $randVal, $file->guessExtension());
											$file->move($savePath, $newName);}
											
							
														$name = $partnersform['name']->getData();
														$text = $partnersform['text']->getData();
														$link = $partnersform['link']->getData();
														$logourl = '/web/uploads/partners/'.$newName;
													
															
															
							
							
							
															
													
															
														$partners->setName($name);
														$partners->setText($text);
														$partners->setLogoUrl($logourl);
												
															
														$em = $this->getDoctrine()->getManager();
														$em->persist($partners);
														$em->flush();
															
														return $this->redirectToRoute('admin-about-us');
													}
														
														
														
													$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Partners');
													$partnersrows = $Repo ->findAll();
			
							
								
							$portfolioform = $this->createFormBuilder()
							->add('name', TextType::class, array(
									'label' => 'Tytuł'))
							->add('city', TextType::class, array(
											'label' => 'Miasto'))
							
							->add('countryid', CountryType::class, array(
													'label' => 'Kraj'))
							->add('year', TextType::class, array(
															'label' => 'Rok'))
							->add('imageurl', FileType::class, array(
													'label' => 'Obrazek'))
							->add('header', TextareaType::class, array(
													'label' => 'Nagłówek',
													'attr' => array('class' => 'ckeditor')
											))
							->add('content', TextareaType::class, array(
													'label' => 'Treść',	
													'attr' => array('class' => 'ckeditor')
											))
							->add('firstfilename', TextType::class, array(
													'label' => 'Nazwa pierwszego pliku',
													'required' => false
							))
							->add('secondfilename', TextType::class, array(
															'label' => 'Nazwa drugiego pliku',
															'required' => false
							))
							->add('thirdfilename', TextType::class, array('label' => 'Nazwa trzeciego pliku',
									'required' => false
							))
							->add('firsturl', FileType::class, array(
													'label' => 'Adres url',
													'required' => false
							))
							->add('secondurl', FileType::class, array(
															'label' => 'Adres url',
															'required' => false
							))
							->add('thirdurl', FileType::class, array('label' => 'Adres url', 'required' => false))
							->add('dodaj', SubmitType::class)
													->getForm();
													$portfolioform->handleRequest($request);
								
								
													if($portfolioform->isValid()){
												
							
			$savePath = $this->get('kernel')->getRootDir().'/../web/uploads/portfolio';

			$formData = $portfolioform->getData();
			unset($formData['imageurl']);
			$randVal = rand(1000,9999);
			
			
			$file = $portfolioform->get('imageurl')->getData();
			if(NULL !=$file){
				$newName = sprintf('img__%d.%s', $randVal, $file->guessExtension());
				$file->move($savePath, $newName);}
				else{
					
				}
			
			
				
				$firstfilename = $portfolioform['firstfilename']->getData();
			$formData = $portfolioform->getData();
			
			unset($formData['firsturl']);
				$randVal = rand(1000,9999);
			
			$file = $portfolioform->get('firsturl')->getData();
			
				if(NULL !=$file){
					if(NULL !=$firstfilename){
					$new1Name = sprintf('first__%d.%s', $randVal, $file->guessExtension());
					$file->move($savePath, $new1Name);}
					else{
						$new1Name = sprintf('first__%d.%s', $randVal, $file->guessExtension());
						$file->move($savePath, $new1Name);
						$firsturl="/web/uploads/portfolio/".$new1Name;
						$firstfilename=$new1Name;}
					
					
					}else{
						
						$firsturl= NULL;
						$firstfilename = NULL;
					}
					

					$secondfilename = $portfolioform['secondfilename']->getData();
					$formData = $portfolioform->getData();
						
					unset($formData['secondurl']);
					$randVal = rand(1000,9999);
						
					$file = $portfolioform->get('secondurl')->getData();
						
					if(NULL !=$file){
						if(NULL !=$secondfilename){
							$new2Name = sprintf('first__%d.%s', $randVal, $file->guessExtension());
							$file->move($savePath, $new2Name);}
							
							else{
								$new2Name = sprintf('first__%d.%s', $randVal, $file->guessExtension());
								$file->move($savePath, $new2Name);
								$secondurl="/web/uploads/portfolio/".$new2Name;
								$secondfilename=$new2Name;}
									
									
					}else{
					
						$secondurl= NULL;
						$secondfilename = NULL;
					}
					$thirdfilename = $portfolioform['thirdfilename']->getData();
					$formData = $portfolioform->getData();
					
					unset($formData['thirdurl']);
					$randVal = rand(1000,9999);
					
					$file = $portfolioform->get('thirdurl')->getData();
					
					if(NULL !=$file){
						if(NULL !=$thirdfilename){
							$new3Name = sprintf('first__%d.%s', $randVal, $file->guessExtension());
							$file->move($savePath, $new3Name);}
								
							else{
								$new3Name = sprintf('first__%d.%s', $randVal, $file->guessExtension());
								$file->move($savePath, $new3Name);
								$thirdurl="/web/uploads/portfolio/".$new3Name;
								$thirdfilename=$new3Name;}
									
									
					}else{
							
						$thirdurl= NULL;
						$thirdfilename = NULL;
					}
					
			
			
	
														
														
														$name = $portfolioform['name']->getData();
														$city = $portfolioform['city']->getData();
														$countryid = $portfolioform['countryid']->getData();		
														$year = $portfolioform['year']->getData();
													
														$header = $portfolioform['header']->getData();
														$content = $portfolioform['content']->getData();
															
														$status = 1;
														$portfolio = new Portfolio;
															
														$portfolio->setName($name);
														$portfolio->setCountryId($countryid);
														$portfolio->setCity($city);
														$portfolio->setYear($year);
														$portfolio->setImageUrl("/web/uploads/portfolio/".$newName);
														$portfolio->setHeader($header);
														$portfolio->setContent($content);
														$portfolio->setFirstFileName($firstfilename);
														$portfolio->setSecondFileName($secondfilename);
														$portfolio->setThirdFileName($thirdfilename);
														$portfolio->setFirstUrl($firsturl);
														$portfolio->setSecondUrl($secondurl);
														$portfolio->setThirdUrl($thirdurl);
														
															
														$em = $this->getDoctrine()->getManager();
														$em->persist($portfolio);
														$em->flush();
															
														return $this->redirectToRoute('admin-about-us');
													}
														
														
														
													$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Portfolio');
													$portfoliorows = $Repo ->findAll();
														
														
							return $this->render('JoblandBundle:Blog:admin/admin-about-us.html.twig', array(
									'mediarows' => $mediarows,
									'mediaform' => $mediaform->createView(),
									'partnersrows' => $partnersrows,
									'partnersform' => $partnersform->createView(),
									'portfoliorows' => $portfoliorows,
									'portfolioform' => $portfolioform->createView()
							));
			
			}

			/**
			 * @Route("/admin/edit-partners/{id}", name="partners_edit")
			 *
			 */
			public function editPartnersAction($id, Request $request) {
				$partners=$this -> getDoctrine()
				->getRepository('JoblandBundle:Partners')
				->find($id);
			
				$partners->setName($partners->getName());
				$path=$partners->getLogoUrl();
			
				$form = $this->createFormBuilder($partners)
			
				->add('name', TextType::class)
				->add('text', TextAreaType::class, array(
										'label' => 'Treść',
										'attr' => array('class' => 'ckeditor')))
				->add('link' , TextType::class)
				->add('logourl', FileType::class, array(
						'label' => 'Wgraj nowe logo',
						'data_class' => null,
						
						'required' => false,
						
				))
				
				->add('save', SubmitType::class)
			
				->getForm();
			
			
				$form->handleRequest($request);
					
			
				if($form->isValid()){
			
					$name = $form['name']->getData();
					$text = $form['text']->getData();
					$link = $form['link']->getData();
			
					$savePath = $this->get('kernel')->getRootDir().'/../web/uploads/partners/';
					
					$formData = $form->getData();
					
					$randVal = rand(1000,9999);
					
					
					
					
					$em = $this->getDoctrine()->getManager();
					$partners = $em->getRepository('JoblandBundle:Partners')->find($id);
			
					$partners->setName($name);
					$partners->setText($text);
					$partners->setLink($link);
					
					$file = $form->get('logourl')->getData();
					if(NULL != $path){
					if(NULL !=$file){
						$newName = sprintf('img__%d.%s', $randVal, $file->guessExtension());
						$file->move($savePath, $newName);
						$path = $this->get('kernel')->getRootDir().'/..'.$path;
						
						
						unlink($path);
						$partners->setLogoUrl('/web/uploads/partners/'.$newName);
					}else{
						$partners->setLogoUrl($path);
					}}else{
						if(NULL !=$file){
							$newName = sprintf('img__%d.%s', $randVal, $file->guessExtension());
							$file->move($savePath, $newName);
							$path = $this->get('kernel')->getRootDir().'/..'.$path;
						
						
							
							$partners->setLogoUrl('/web/uploads/partners/'.$newName);
						}else{
							$partners->setLogoUrl($path);
						
					}}
			
			
					$em->flush();
			
			
					return $this->redirectToRoute('admin-about-us');
				}
			
				return $this->render('JoblandBundle:Blog:admin/edit-category.html.twig', array(
						'partners' => $partners,
						'form' => $form->createView()
				));
			
			}
				
			
			
			
	/**
     * @Route("/admin/edit-category/{id}", name="category_edit")
     *
     */
	public function editcategoryAction($id, Request $request) {
		$category =$this -> getDoctrine()
		 ->getRepository('JoblandBundle:Category')
		 ->find($id);

		  	$category->setName($category->getName());
		    

		    	$form = $this->createFormBuilder($category)
		 	
			->add('name', TextType::class)
			->add('save', SubmitType::class)
		    
		    	->getForm();


			$form->handleRequest($request);
			

		if($form->isValid()){

		$name = $form['name']->getData();
		
	
		$em = $this->getDoctrine()->getManager();
		$category = $em->getRepository('JoblandBundle:Category')->find($id);

		$category->setName($name);
		

		$em->flush();
		
	
		return $this->redirectToRoute('admin-category');	
			}

	return $this->render('JoblandBundle:Blog:admin/edit-category.html.twig', array(
		'category' => $category,
		'form' => $form->createView()
		));
	
	}
	
	/**
	 * @Route("/admin/edit-media/{id}", name="media_edit")
	 *
	 */
	public function editMediaAction($id, Request $request) {
		$media =$this -> getDoctrine()
		->getRepository('JoblandBundle:Media')
		->find($id);
	
		
	
	
		$form = $this->createFormBuilder($media)
	
		->add('title', TextType::class)
		->add('url', TextType::class)
		->add('status', TextType::class)
		->add('content', TextareaType::class, array(
				'label' => 'Treść',
				'attr' => array('class' => 'ckeditor')))
		->add('date', TextType::class, array(
						'label' => 'Date'	))
		->add('save', SubmitType::class)
	
		->getForm();
	
	
		$form->handleRequest($request);
			
	
		if($form->isValid()){
	
			$title = $form['title']->getData();
			$date = $form['date']->getData();
			$content = $form['content']->getData();
			$url = $form['url']->getData();
			$status = $form['status']->getData();
	
	
			$em = $this->getDoctrine()->getManager();
			$media = $em->getRepository('JoblandBundle:Media')->find($id);
	
			$media->setTitle($title);
			$media->setContent($content);
			$media->setUrl($url);
			$media->setStatus($status);
			$media->setDate($date);
	
	
			$em->flush();
	
	
			return $this->redirectToRoute('admin-about-us');
		}
	
		return $this->render('JoblandBundle:Blog:admin/edit-category.html.twig', array(
				'media' => $media,
				'form' => $form->createView()
		));
	
	}
	
	/**
	 * @Route("/admin/edit-portfolio/{id}", name="portfolio_edit")
	 *
	 */
	public function editPortfolioAction($id, Request $request) {
		$portfolio =$this -> getDoctrine()
		->getRepository('JoblandBundle:Portfolio')
		->find($id);
	
	
	
	
		$form = $this->createFormBuilder($portfolio)
	
		->add('name', TextType::class)
		->add('city', TextType::class)
		->add('country_id', CountryType::class)
		->add('year', TextType::class)
		->add('header', TextareaType::class, array(
				'label' => 'Treść',
				'attr' => array('class' => 'ckeditor')))
		->add('content', TextareaType::class, array(
				'label' => 'Treść',
				'attr' => array('class' => 'ckeditor')))
		->add('save', SubmitType::class)
	
						->getForm();
	
	
						$form->handleRequest($request);
							
	
						if($form->isValid()){
	
							$name = $form['name']->getData();
							$city = $form['city']->getData();
							$countryid=$form['country_id']->getData();
							$header=$form['header']->getData();
							$content = $form['content']->getData();
							$year=$form['year']->getData();
							
	
							$em = $this->getDoctrine()->getManager();
							$portfolio = $em->getRepository('JoblandBundle:Portfolio')->find($id);
	
							$portfolio->setName($name);
							$portfolio->setHeader($header);
							$portfolio->setContent($content);
							$portfolio->setCountryId($countryid);
							$portfolio->setCity($city);
							$portfolio->setYear($year);
						
	
	
							$em->flush();
	
	
							return $this->redirectToRoute('admin-about-us');
						}
	
						return $this->render('JoblandBundle:Blog:admin/edit-category.html.twig', array(
								'portfolio' => $portfolio,
								'form' => $form->createView()
						));
	
	}
	

	/**
     * @Route("/admin/edit-newsletter/{id}", name="newsletter_edit")
     *
     */
	public function editnewsletterAction($id, Request $request) {
		$newsletter =$this -> getDoctrine()
		 ->getRepository('JoblandBundle:Newsletter')
		 ->find($id);

		  	$newsletter->setEmail($newsletter->getEmail());
		    $newsletter->setStatus($newsletter->getStatus());

		    	$form = $this->createFormBuilder($newsletter)
		 	->add('email', EmailType::class)
			->add('status', TextType::class)
			->add('save', SubmitType::class)
		    
		    	->getForm();


			$form->handleRequest($request);
			

		if($form->isValid()){

		$email = $form['email']->getData();
		$status = $form['status']->getData();
	
		$em = $this->getDoctrine()->getManager();
		$newsletter = $em->getRepository('JoblandBundle:Newsletter')->find($id);

		$newsletter->setEmail($email);
		$newsletter->setStatus($status);

		$em->flush();
		
	
		return $this->redirectToRoute('admin-newsletter');	
			}



	return $this->render('JoblandBundle:Blog:admin/edit-newsletter.html.twig', array(
		'newsletter' => $newsletter,
		'form' => $form->createView()
		));
	
	}
	
	
	/**
	 * @Route("/admin/edit-news/{id}", name="news_edit")
	 *
	 */
	public function editnewsAction($id, Request $request) {
		$news =$this -> getDoctrine()
		->getRepository('JoblandBundle:News')
		->find($id);
	
		$news->setTitle($news->getTitle());
		$news->setContent($news->getContent());
		$news->setDate($news->getDate()->format('d-m-Y'));
		$news->setExcerpt($news->getExcerpt());
	
		$form = $this->createFormBuilder($news)
		->add('title', TextType::class, array(
				'label' => 'Tytuł'))
		->add('excerpt', TextType::class, array(
						'label' => 'Zajawka'))
		->add('date', TextType::class, array(
								'label' => 'Data'))
		->add('content', TextareaType::class, array(
										'label' => 'Treść',
										'attr' => array('class' => 'ckeditor')))
										->add('save', SubmitType::class)
	
		->getForm();
	
	
		$form->handleRequest($request);
			
	
		if($form->isValid()){
	
			$title = $form['title']->getData();
			$content = $form['content']->getData();
			$excerpt = $form['excerpt']->getData();
			$date = $form['date']->getData();
			
			$em = $this->getDoctrine()->getManager();
			$news = $em->getRepository('JoblandBundle:news')->find($id);
	
			$news->setTitle($title);
			$news->setContent($content);
			$news_>setExcerpt($excerpt);
			$news->setDate(new \DateTime($date));
			
	
			$em->flush();
	
	
			return $this->redirectToRoute('admin-news');
		}
	
	
	
		return $this->render('JoblandBundle:Blog:admin/edit-news.html.twig', array(
				'news' => $news,
				'form' => $form->createView()
		));
	
	}

	/**
     * @Route("/admin/edit-employer/{id}", name="employer_edit")
     *
     */
	public function edituserAction($id, Request $request) {
		$user =$this -> getDoctrine()
		 ->getRepository('JoblandBundle:User')
		 ->find($id);

		  	$user->setCompany($user->getCompany());
		    $user->setPhoneNumber($user->getPhoneNumber());
		    $user->setEmail($user->getEmail());
		    $user->setPassword($user->getPassword());
		    $user->setNip($user->getNip());
		    $user->setContactPerson($user->getContactPerson());
		    $user->setUrl($user->getUrl());
		    $user->setAddress($user->getAddress());
		    $user->setPostalCode($user->getPostalCode());
		    $user->setCity($user->getCity());
		  
		  

		 	$form = $this->createFormBuilder($user)
		 	->add('company', TextType::class)
			->add('email', EmailType::class)
			->add('password', TextType::class)
			->add('phoneNumber', TextType::class)
			->add('nip', TextType::class)
			->add('url', TextType::class)
			->add('address', TextType::class)
			->add('postalCode', TextType::class)
			->add('city', TextType::class)
			->add('contactperson', TextType::class)
			->add('save', SubmitType::class)

				->getForm();


			$form->handleRequest($request);
			

		if($form->isValid()){

		$company = $form['company']->getData();
		$phoneNumber = $form['phoneNumber']->getData();
		$email = $form['email']->getData();
		$password = $form['password']->getData();
		$nip = $form['nip']->getData();
		$name = $form['contactperson']->getData();
		$url = $form['url']->getData();
		$adress = $form['address']->getData();
		$city = $form['city']->getData();
		$postcode = $form['postalCode']->getData();

		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository('JoblandBundle:user')->find($id);

		$user->setCompany($company);
		$user->setPhoneNumber($phoneNumber);
		$user->setEmail($email);
		$user->setPassword($password);
		$user->setNip($nip);
		$user->setContactPerson($name);
		$user->setUrl($url);
		$user->setPostalCode($postcode);
		$user->setAddress($adress);
		$user->setCity($city);

		$em->flush();
		
	
		return $this->redirectToRoute('admin-employers');	
			}



	return $this->render('JoblandBundle:Blog:admin/edit-employer.html.twig', array(
		'user' => $user,
		'form' => $form->createView()
		));
	
	}

	/**
     * @Route("/admin/edit-offer/{id}", name="offer_edit")
     *
     */
	public function editOfferAdminAction($id, Request $request) {
		$offer =$this -> getDoctrine()
		 ->getRepository('JoblandBundle:Offer')
		 ->find($id);

		  	$offer->setName($offer->getName());
		    $offer->setUserId($offer->getUserId());
		    $offer->setCountryId($offer->getCountryId());
		    $offer->setEmployer($offer->getEmployer());
		    $offer->setJobDescription($offer->getJobDescription());
		    $offer->setRequirements($offer->getRequirements());
		    $offer->setApplyOffer($offer->getApplyOffer());
		    $offer->setApplyAdress($offer->getApplyAdress());
		    $offer->setOfferStatus($offer->getOfferStatus());
		    $offer->setCategoryId($offer->getCategoryId());
		    $offer->setOfferCategorId($offer->getOfferCategorId());

		 $Repo = $this->getDoctrine()->getRepository('JoblandBundle:Category');
		$rows = $Repo ->findAll();


$choices = array();
foreach ($rows as $value) {
	$choices[$value->getName()] = $value->getId();
}

		 	$form = $this->createFormBuilder($offer)
				->add('name', TextType::class)
				->add('userid', TextType::class)
				->add('countryid', CountryType::class)
				->add('categoryid', ChoiceType::class ,array(
					'label' => 'Branża',
					'choices' => $choices))
				->add('offercategorid', ChoiceType::class, array(
					'label' => 'Branża',
					'choices' => $choices))
				->add('jobdescription', TextAreaType::class)
				->add('employer', TextAreaType::class)
				->add('requirements', TextAreaType::class)
				->add('applyoffer', TextAreaType::class)
				->add('applyadress', EmailType::class)
				->add('offerstatus', TextType::class)
				->add('editoffer', SubmitType::class)
					->getForm();
						


			$form->handleRequest($request);
			

		if($form->isValid()){

		$name = $form['name']->getData();
		$userid = $form['userid']->getData();
		$countryid = $form['countryid']->getData();
		$categoryid = $form['categoryid']->getData();
		$offercategorid = $form['offercategorid']->getData();
		$jobdescription = $form['jobdescription']->getData();
		$employer = $form['employer']->getData();
		$requirements = $form['requirements']->getData();
		$applyoffer = $form['applyoffer']->getData();
		$applyadress = $form['applyadress']->getData();
		$offerstatus = $form['offerstatus']->getData();

		 
       

       
		$em = $this->getDoctrine()->getManager();
		$offer = $em->getRepository('JoblandBundle:Offer')->find($id);

		$offer->setName($name);
		$offer->setUserId($userid);
		$offer->setCountryId($countryid);
		$offer->setCategoryId($categoryid);
		$offer->setOfferCategorId($offercategorid);
		$offer->setJobDescription($jobdescription);
		$offer->setEmployer($employer);
		$offer->setRequirements($requirements);
		$offer->setApplyOffer($applyoffer);
		$offer->setApplyAdress($applyadress);
		$offer->setOfferStatus($offerstatus);
		
		

		$em->flush();
		
	
		return $this->redirectToRoute('admin-offers');	
			}



	return $this->render('JoblandBundle:Blog:admin/edit-offer.html.twig', array(
		'offer' => $offer,
		'form' => $form->createView()
		));
	
	}

	
	
	/**
	 * @Route("/admin/delete-news/{id}", name="news_delete")
	 *
	 */
	public function deleteNewsAction($id) {
		$em = $this->getDoctrine()->getManager();
		$news = $em->getRepository('JoblandBundle:News')->findOneById($id);
	
		$em->remove($news);
		$em->flush();
	
		return $this->redirectToRoute('admin-news');
	}
	
	/**
	 * @Route("/admin/delete-partners/{id}", name="partners-delete")
	 *
	 */
	public function deletePartnersAction($id) {
		$em = $this->getDoctrine()->getManager();
		$partners = $em->getRepository('JoblandBundle:Partners')->findOneById($id);
		$partnerspath = $partners->getLogourl();
		$deletePath = $this->get('kernel')->getRootDir().'/..'.$partnerspath;
		
		unlink($deletePath);
		
		$em->remove($partners);
		$em->flush();
	
		return $this->redirectToRoute('admin-about-us');
	}

	
	/**
	 * @Route("/save-newsletter/{email}", name="offer_delete")
	 *
	 */
	public function savenewsletterAction($email) {
// 		$em = $this->getDoctrine()->getManager();
// 		$newsletter = $em->getRepository('JoblandBundle:Newsletter')->findOneByEmail($email);
	
// 		$em->remove($newsletter);
// 		$em->flush();
	
		$newsletter = new Newsletter;
		
		
					$name = $email;
		
					$newsletter->setEmail($name);
					$newsletter->setStatus('1');
					
		
					$em = $this->getDoctrine()->getManager();
					$em->persist($newsletter);
					$em->flush();
		
					return $this->redirectToRoute('homepage');
				}
				
	/**
	* @Route("/delete-newsletter/{email}", name="delete-newsletter")
	*
	*/
	public function deletenewsletterAction($email) {
					 		$em = $this->getDoctrine()->getManager();
					 		$newsletter = $em->getRepository('JoblandBundle:Newsletter')->findOneByEmail($email);
				
					 		$em->remove($newsletter);
					 		$em->flush();
				
					
					return $this->redirectToRoute('homepage');
				}
	/**
	 * @Route("/admin/active-offers", name="adminoffer_active")
	 *
	 */
	public function adminActiveOfferAction() {
		$em = $this->getDoctrine()->getManager();
		$offer = $em->getRepository('JoblandBundle:Offer')->findByOfferStatus('2');
		
		//echo '<pre>'; print_r($offer);
		//die();
		
		//$status = array();
		foreach ($offer as $value) {
			$value->setOfferStatus('1');
			$em->persist($value);	
		}
	
		$em->flush();
	
		return $this->redirectToRoute('admin-offers');
	}
	/**
	 * @Route("/admin/active-offer/{id}", name="admin-active-offer")
	 *
	 */
	public function adminActivedplayOfferAction($id) {
		$em = $this->getDoctrine()->getManager();
		$offer = $em->getRepository('JoblandBundle:Offer')->find($id);
		$offer->setOfferStatus('1');
	
	
	
		$em->flush();
	
		return $this->redirectToRoute('admin-offers');
	}
	
	/**
	 * @Route("/admin/inactive-newsletter/{id}", name="adminnewsletter_inactive")
	 *
	 */
	public function adminInactiveAction($id) {
		$em = $this->getDoctrine()->getManager();
		$newsletter = $em->getRepository('JoblandBundle:Newsletter')->find($id);
		$newsletter->setStatus('2');
		
		
		
		$em->flush();
		
		return $this->redirectToRoute('admin-newsletter');
	}
	
	/**
	 * @Route("admin/active-newsletter/{id}", name="adminnewsletter_active")
	 *
	 */
	public function adminActiveAction($id) {
		$em = $this->getDoctrine()->getManager();
		$newsletter = $em->getRepository('JoblandBundle:Newsletter')->find($id);
		$newsletter->setStatus('1');
	
	
	
		$em->flush();
	
		return $this->redirectToRoute('admin-newsletter');
	}
	
	
	/**
	 * @Route("/panel-pracodawcy/inactive-offer/{id}", name="offer_inactive")
	 *
	 */
	public function inactiveOfferAction($id) {
		$em = $this->getDoctrine()->getManager();
		$offer = $em->getRepository('JoblandBundle:Offer')->find($id);
		$offer->setOfferStatus('3');
		
	
		
		$em->flush();
	
		return $this->redirectToRoute('moje-oferty');
	}
	
	/**
	 * @Route("/panel-pracodawcy/renew-offer/{id}", name="offer_renew")
	 *
	 */
	public function renewOfferAction($id) {
		$em = $this->getDoctrine()->getManager();
		$offer = $em->getRepository('JoblandBundle:Offer')->find($id);
		$offer->setOfferStatus('2');
	
	
		$em->flush();
	
		return $this->redirectToRoute('moje-oferty');
	}

	/**
     * @Route("/admin/delete-employer/{id}", name="employer_delete")
     *
     */
	public function deleteEmployerAction($id) {
		$em = $this->getDoctrine()->getManager();
		$employer = $em->getRepository('JoblandBundle:Employer')->find($id);

		$em->remove($employer);
		$em->flush();

	return $this->redirectToRoute('admin-employers');	
	}
	/**
	 * @Route("/admin/delete-news/{id}", name="news_delete")
	 *
	 */
	public function deleteAdminNewsAction($id) {
		$em = $this->getDoctrine()->getManager();
		$news = $em->getRepository('JoblandBundle:News')->find($id);
	
		$em->remove($news);
		$em->flush();
	
		return $this->redirectToRoute('admin-news');
	}
	/**
	 * @Route("/admin/delete-offer/{id}", name="news_delete")
	 *
	 */
	public function deleteOfferAction($id) {
		$em = $this->getDoctrine()->getManager();
		$offer = $em->getRepository('JoblandBundle:Offer')->find($id);
	
		$em->remove($offer);
		$em->flush();
	
		return $this->redirectToRoute('admin-offers');
	}
	
	/**
	 * @Route("/english", name="english_button", defaults = {"_locale" = "en"})
	 *
	 */
	public function englishButtonAction(Request $request) {
		echo 'przed ';
		echo $this->container->getParameter('_locale');
		echo $this->container->setParameter('_locale', 'en');
		
		
		die();
	
		
		
		
	
		return $this->redirectToRoute('homepage');
	}
	
	/**
     * @Route("/admin/delete-newsletter/{id}", name="newsletter_delete")
     *
     */
	public function deleteAdminNewsletterAction($id) {
		$em = $this->getDoctrine()->getManager();
		$newsletter = $em->getRepository('JoblandBundle:Newsletter')->find($id);

		$em->remove($newsletter);
		$em->flush();

	return $this->redirectToRoute('admin-newsletter');	
	}
	/**
     * @Route("/admin/delete-category/{id}", name="category_delete")
     *
     */
	public function deletecategoryAction($id) {
		$em = $this->getDoctrine()->getManager();
		$category = $em->getRepository('JoblandBundle:Category')->find($id);

		$em->remove($category);
		$em->flush();

	return $this->redirectToRoute('admin-category');	
	}



	

	

	
	
	/**
     * @Route("/admin/offers", name="admin-offers")
     *
     */
	public function adminoffersAction(Request $request) {
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Category');
		$jobs = $Repo ->findAll();
		
		
		$choices = array();
		foreach ($jobs as $value) {
			$choices[$value->getName()] = $value->getId();
		}
		
		$offer = new  Offer;
		$form = $this->createFormBuilder($offer)
				->add('name', TextType::class)
				->add('city', TextType::class)
				->add('countryid', CountryType::class)
				->add('categoryid', ChoiceType::class ,array(
						'label' => 'Branża',
						'choices' => $choices))
				->add('offercategorid', ChoiceType::class, array(
								'label' => 'Branża',
								'choices' => $choices))
				->add('jobdescription', TextAreaType::class)
				->add('employer', TextAreaType::class)
				->add('requirements', TextAreaType::class)
				->add('applyoffer', TextAreaType::class)
				->add('applyadress', EmailType::class)
				->add('addoffer', SubmitType::class)
				->getForm();
				
				$form->handleRequest($request);
					
				
		
				if($form->isValid()){
		
					$name = $form['name']->getData();
					$city = $form['city']->getData();
					$countryid = $form['countryid']->getData();
					$categoryid = $form['categoryid']->getData();
					$offercategorid = $form['offercategorid']->getData();
					$employer = $form['employer']->getData();
					$requirements = $form['requirements']->getData();
					$jobdescription = $form['jobdescription']->getData();
					$applyoffer = $form['applyoffer']->getData();
					$applyadress = $form['applyadress']->getData();
					
					
					$offer->setName($name);
					$offer->setCity($city);
					$offer->setUserId($this->getUser()->getId());
					$offer->setCountryId($countryid);
					$offer->setCategoryId($categoryid);
					$offer->setOfferCategorId($offercategorid);
					$offer->setEmployer($employer);
					$offer->setRequirements($requirements);
					$offer->setJobDescription($jobdescription);
					$offer->setApplyOffer($applyoffer);
					$offer->setApplyAdress($applyadress);
					$offer->setOfferStatus('2');
		
					$em = $this->getDoctrine()->getManager();
					$em->persist($offer);
					$em->flush();
		
					return $this->redirectToRoute('admin-offers');
				}
				
	$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
	$rows = $Repo ->findAll();


	return $this->render('JoblandBundle:Blog:admin/admin-offers.html.twig', array(
		'rows' => $rows,
		'jobs' => $jobs,	
		'form' => $form->createView()
		));
		
	}
	
	/**
     * @Route("/admin/employers", name="admin-employers")
     *
     */
	public function adminemployersAction() {
	$Repo = $this->getDoctrine()->getRepository('JoblandBundle:User');
	$rows = $Repo ->findAll();


	return $this->render('JoblandBundle:Blog:admin/admin-employers.html.twig', array(
		'rows' => $rows
		));
		
	}

	/**
     * @Route("/admin/newsletter", name="admin-newsletter")
     *
     */
	public function adminnewsletterAction(Request $request) {
	
		$newsletter = new Newsletter;
		
		$form = $this->createFormBuilder($newsletter)
		->add('email', TextType::class, array(
				'label' => 'Dodaj Email'))
				->add('add', SubmitType::class)
				->getForm();
				$form->handleRequest($request);
					
		
				if($form->isValid()){
		
					$email = $form['email']->getData();
		
					$newsletter->setEmail($email);
					$newsletter->setStatus('1');
		
					$em = $this->getDoctrine()->getManager();
					$em->persist($newsletter);
					$em->flush();
		
					return $this->redirectToRoute('admin-newsletter');
				}
		
	$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Newsletter');
	$rows = $Repo ->findAll();


	return $this->render('JoblandBundle:Blog:admin/admin-newsletter.html.twig', array(
		'rows' => $rows,
		'form' => $form->createView()
		));
		
	}

	/**
     * @Route("/admin/category", name="admin-category")
     *
     */
	public function admincategoryAction(Request $request) {
	

	$category = new  Category;

	$form = $this->createFormBuilder($category)
	->add('name', TextType::class, array(
		'label' => 'Dodaj Kategorię'))
	->add('add', SubmitType::class)
	->getForm();
	$form->handleRequest($request);
			

		if($form->isValid()){

		$name = $form['name']->getData();

		$category->setName($name);

		$em = $this->getDoctrine()->getManager();
		$em->persist($category);
		$em->flush();
		
		
		
		return $this->redirectToRoute('admin-category');	
		}



	$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Category');
	$rows = $Repo ->findAll();
	$paginator = $this->get('knp_paginator');
	$pagination = $paginator->paginate(
			$rows,
			$request->query->get('page', 1),
			10
			);


	return $this->render('JoblandBundle:Blog:admin/admin-category.html.twig', array(
		'rows' => $pagination,
		'form' => $form->createView()
		));
		
	}
	
	/**
	 * @Route("/admin/news", name="admin-news")
	 *
	 */
	public function adminnewsAction(Request $request) {
	
	
		$news = new  News;
	
		$form = $this->createFormBuilder($news)
		->add('title', TextType::class, array(
				'label' => 'Tytuł'))
		->add('excerpt', TextType::class, array(
						'label' => 'Zajawka'))	
		->add('date', TextType::class, array(
								'label' => 'Data'))
		->add('content', TextareaType::class, array(
						'label' => 'Treść',
						'attr' => array('class' => 'ckeditor')
		))
		
				->add('add', SubmitType::class)
				->getForm();
				$form->handleRequest($request);
					
	
				if($form->isValid()){
	
					$title = $form['title']->getData();
					$content = $form['content']->getData();
					$excerpt = $form['excerpt']->getData();
					$date = $form['date']->getData();;
					$status = 2;
					
					$news->setTitle($title);
					$news->setContent($content);
					$news->setExcerpt($excerpt);
					$news->setStatus($status);
					$news->setDate(new \DateTime($date));
	
					$em = $this->getDoctrine()->getManager();
					$em->persist($news);
					$em->flush();
	
					return $this->redirectToRoute('admin-news');
				}
	
	
	
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:News');
				$rows = $Repo ->findAll();
	
	
				return $this->render('JoblandBundle:Blog:admin/admin-news.html.twig', array(
						'rows' => $rows,
						'form' => $form->createView()
				));
	
	}

	
	/**
     * @Route("/panel-pracodawcy/dodaj-oferte")
     *
     */
	public function addofferAction(Request $Request) {
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Category');
		$rows = $Repo ->findAll();


$choices = array();
foreach ($rows as $value) {
	$choices[$value->getName()] = $value->getId();
}

		$form = $this->createFormBuilder()
		->add('positionname', TextType::class)
		->add('city', TextType::class)
		->add('country', CountryType::class)
		->add('businesssector1', ChoiceType::class ,array(
			'label' => 'Branża',
			'choices' => $choices))
		->add('businesssector2', ChoiceType::class, array(
			'label' => 'Branża',
			'choices' => $choices))
		->add('description', TextAreaType::class)
		->add('duties', TextAreaType::class)
		->add('requirements', TextAreaType::class)
		->add('offer', TextAreaType::class)
		->add('howtoapply', EmailType::class)
		->add('addoffer', SubmitType::class)
			->getForm();


			$form->handleRequest($Request);
			

		if($form->isValid()){

			
			$formData = $form->getData();

			
			
			
	//$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Category');
	//$rows = $Repo ->findAll();
		
		
           
    $offer = new Offer();
    $offer->setName($formData['positionname']);
    $offer->setCity($formData['city']);
    $offer->setUserId($this->getUser()->getId());
    $offer->setCountryId($formData['country']);
    $offer->setEmployer($formData['duties']);
    $offer->setJobDescription($formData['description']);
    $offer->setRequirements($formData['requirements']);
    $offer->setApplyOffer($formData['offer']);
    $offer->setApplyAdress($formData['howtoapply']);
    $offer->setOfferStatus('2');
    $offer->setCategoryId($formData['businesssector1']);
    $offer->setOfferCategorId($formData['businesssector2']);


    

    $em = $this->getDoctrine()->getManager();

    // tells Doctrine you want to (eventually) save the Product (no queries yet)
    $em->persist($offer);

    // actually executes the queries (i.e. the INSERT query)
    $em->flush();

    

			
			
			$formData='Zapisano Dane formularza';
		}
	

		return $this->render('JoblandBundle:Blog:employer-panel/add-offer.html.twig', array(
			'form' => $form->createView(),
			'formData' => isset($formData) ? $formData : NULL,
		));
	
	}
	/**
     * @Route("/panel-pracodawcy/edytuj-oferte/{id}")
     *
     */
	public function editofferAction($id, Request $request) {
		$offer =$this -> getDoctrine()
		 ->getRepository('JoblandBundle:Offer')
		 ->find($id);

		  	$offer->setName($offer->getName());
		    $offer->setCity($offer->getCity());
		    $offer->setCountryId($offer->getCountryId());
		    $offer->setEmployer($offer->getEmployer());
		    $offer->setJobDescription($offer->getJobDescription());
		    $offer->setRequirements($offer->getRequirements());
		    $offer->setApplyOffer($offer->getApplyOffer());
		    $offer->setApplyAdress($offer->getApplyAdress());
		    $offer->setCategoryId($offer->getCategoryId());
		    $offer->setOfferCategorId($offer->getOfferCategorId());

		 $Repo = $this->getDoctrine()->getRepository('JoblandBundle:Category');
		$rows = $Repo ->findAll();


$choices = array();
foreach ($rows as $value) {
	$choices[$value->getName()] = $value->getId();
}

		 	$form = $this->createFormBuilder($offer)
				->add('name', TextType::class)
				->add('city', TextType::class)
				->add('countryid', CountryType::class)
				->add('categoryid', ChoiceType::class ,array(
					'label' => 'Branża',
					'choices' => $choices))
				->add('offercategorid', ChoiceType::class, array(
					'label' => 'Branża',
					'choices' => $choices))
				->add('jobdescription', TextAreaType::class)
				->add('employer', TextAreaType::class)
				->add('requirements', TextAreaType::class)
				->add('applyoffer', TextAreaType::class)
				->add('applyadress', EmailType::class)
				->add('editoffer', SubmitType::class)
					->getForm();
						


			$form->handleRequest($request);
			

		if($form->isValid()){

		$name = $form['name']->getData();
		$city = $form['city']->getData();
		$countryid = $form['countryid']->getData();
		$categoryid = $form['categoryid']->getData();
		$offercategorid = $form['offercategorid']->getData();
		$jobdescription = $form['jobdescription']->getData();
		$employer = $form['employer']->getData();
		$requirements = $form['requirements']->getData();
		$applyoffer = $form['applyoffer']->getData();
		$applyadress = $form['applyadress']->getData();
		

		 
       

       
		$em = $this->getDoctrine()->getManager();
		$offer = $em->getRepository('JoblandBundle:Offer')->find($id);

		$offer->setName($name);
		$offer->setCity($city);
		$offer->setCountryId($countryid);
		$offer->setCategoryId($categoryid);
		$offer->setOfferCategorId($offercategorid);
		$offer->setJobDescription($jobdescription);
		$offer->setEmployer($employer);
		$offer->setRequirements($requirements);
		$offer->setApplyOffer($applyoffer);
		$offer->setApplyAdress($applyadress);
	
		
		

		$em->flush();
		
	
		return $this->redirectToRoute('admin-offers');	
			}



	return $this->render('JoblandBundle:Blog:employer-panel/edit-offer.html.twig', array(
		'offer' => $offer,
		'form' => $form->createView()
		));
	
	}

	
	
	/**
     * @Route("/panel-pracodawcy/moje-oferty", name="moje-oferty")
     *
     */
	public function offersAction() {
	$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
	
	/*print_r($this->getUser());*/
	$user = $this->getUser()->getId();
	
	$offers = $Repo ->findByUserId($user);



	
	return $this->render('JoblandBundle:Blog:employer-panel/offers.html.twig', array(
			'offers' => $offers,
			
		));	
	}
	
	/**
	 * @Route("/works-offer/", name="search")
	 *
	 */
	public function SearchAction(Request $request){
		
		$post = $request->get('search');
		
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:User');
		$user = $Repo ->findAll();
		 
		
		
// 		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
// 		$rows = $Repo ->findBy(array(
// 				'offerStatus' => 1,
// 				'name' => $post['query']
// 		));
// 		$paginator = $this->get('knp_paginator');
// 		$pagination = $paginator->paginate(
// 				$rows,
// 				$request->query->get('page', 1),
// 				5
// 				);
		 $em = $this->getDoctrine()->getManager();
		 $query = $em->createQuery(
		 		'SELECT p
    			FROM JoblandBundle:Offer p
    			WHERE (p.countryId LIKE :country AND (p.name LIKE :name OR p.city LIKE :name OR p.employer LIKE :name OR p.jobDescription LIKE :name OR p.requirements LIKE :name OR p.applyOffer LIKE :name))
    			ORDER BY p.id DESC'
		 		)->setParameter('name', '%'.$post['query'].'%')
		 		->setParameter('country', '%'.$post['country'].'%');
		 
		 		$rows = $query->getResult();
		 		$paginator = $this->get('knp_paginator');
		 		$pagination = $paginator->paginate(
		 				$rows,
		 				$request->query->get('page', 1),
		 				5
		 				);
		return $this->render('JoblandBundle:Blog:for-candidates/works-offer.html.twig', array(
				'rows' => $pagination,
				'user'=> $user
		));
		
		}
	
	/**
     * @Route("/panel-pracodawcy/panel-glowny", name="employer-panel")
     *
     */
	public function employerpanelAction(Request $Request) {
		
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:User');
		
		
		$test = $this->getUser()->getId();
		
		$userid = $Repo ->findById($test);
		
		
		$form = $this->createFormBuilder()
		
		->add('logo', FileType::class, array(
				'label' => 'Nie posiadasz loga. '))
		->add('Dodaj', SubmitType::class)
		
		->getForm();
		
		
		$form->handleRequest($Request);
		
		if($form->isValid()){
		
			$savePath = $this->get('kernel')->getRootDir().'/../web/uploads/user/profilepic';
		
			$formData = $form->getData();
			unset($formData['logo']);
			$randVal = rand(1000,9999);
			$dataFileName = sprintf('data_%d.txt', $randVal);
			file_put_contents($savePath.$dataFileName, print_r($formData, TRUE));
				
			$file = $form->get('logo')->getData();
			if(NULL !=$file){
				$newName = sprintf('file__%d.%s', $randVal, $file->guessExtension());
				$file->move($savePath, $newName);
			}
		
			
			
			$em = $this->getDoctrine()->getManager();
			$id = $this->getUser()->getId();
			$user = $em->getRepository('JoblandBundle:User')->find($id);
		
		
		
			 
			
			$user->setprofilePicturePath("/web/uploads/user/profilepic/"."$newName");
		
			
		
			
			$em->flush();
			return $this->redirectToRoute('employer-panel');
		
		
		}
		
	return $this->render('JoblandBundle:Blog:employer-panel/employer-panel.html.twig', array(
			'userid' => $userid,
			'form' => $form->createView()
	));	
	}
	/**
	 * @Route("/employer-panel/delete-logo/{id}", name="news_delete")
	 *
	 */
	public function deleteLogoAction($id) {
		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository('JoblandBundle:User')->find($id);
		$user->setprofilePicturePath('');
		$profilePicturePath = $user->getprofilePicturePath();
		$savePath = $this->get('kernel')->getRootDir().'/..'.$profilePicturePath;
		
		unlink($savePath);
		
		
		
		$em->flush();
		
	
		return $this->redirectToRoute('employer-panel');
	}
	
	
	/**
     * @Route("/panel-pracodawcy/moje-dane")
     *
     */
	public function mydateAction() {
	return $this->render('JoblandBundle:Blog:employer-panel/my-date.html.twig', array());	
	}
	/**
     * @Route("/zamieszczanie-ofert")
     *
     */
	public function addnewofferAction() {
	return $this->render('JoblandBundle:Blog:for-employers/add-new-offer.html.twig', array());	
	}
	/**
     * @Route("/jak-dzialamy")
     *
     */
	public function howweworksAction() {
	return $this->render('JoblandBundle:Blog:for-employers/how-we-works.html.twig', array());	
	}
	/**
     * @Route("/newsletter")
     *
     */
	public function newsletterAction() {
	return $this->render('JoblandBundle:Blog:for-employers/newsletter.html.twig', array());	
	}
	/**
     * @Route("/branza")
     *
     */
	public function tradeAction() {
	return $this->render('JoblandBundle:Blog:for-employers/trade.html.twig', array());	
	}
	/**
     * @Route("/rejestracja")
     *
     */
	public function registeremployerAction(Request $Request) {

		$form = $this->createFormBuilder()
		->add('email', RepeatedType::class, array(
				'type' => TextType::class,
				'invalid_message' => 'The password fields must match.',
				'required' => true,
				'first_options'  => array(),
				'second_options' => array(),
		))
		->add('passwd', RepeatedType::class, array(
				'type' => PasswordType::class,
				'invalid_message' => 'The password fields must match.',
				'required' => true,
				'first_options'  => array(),
				'second_options' => array(),
		))
		->add('company_name', TextType::class)
		->add('phone', TextType::class)
		->add('nip', TextType::class)
		->add('url', TextType::class)
		->add('adress', TextType::class)
		->add('postcode', TextType::class)
		->add('city', TextType::class)
		->add('country', CountryType::class)
		->add('name', TextType::class)
		->add('logo', FileType::class)
		->add('agreement', CheckboxType::class)
		->add('rules', CheckboxType::class)
		->add('save', SubmitType::class)

			->getForm();


			$form->handleRequest($Request);
		
		if($form->isValid()){

			$savePath = $this->get('kernel')->getRootDir().'/../web/uploads';

			$formData = $form->getData();
			unset($formData['logo']);
			$randVal = rand(1000,9999);
			$dataFileName = sprintf('data_%d.txt', $randVal);
			file_put_contents($savePath.$dataFileName, print_r($formData, TRUE));
			
			$file = $form->get('logo')->getData();
			if(NULL !=$file){
				$newName = sprintf('file__%d.%s', $randVal, $file->guessExtension());
				$file->move($savePath, $newName);
			}

			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Country');
           $country = $Repo->find(1);
   
    $passwd = (md5($formData['passwd']));
    $Repo = $this->getDoctrine()->getRepository('JoblandBundle:Employer');
    $name = $Repo->find($formData['email']);
  
  
           
    $employer = new Employer();
    $employer->setCompanyName($formData['company_name']);
    $employer->setPhoneNumber($formData['phone']);
    $employer->setEmail($formData['email']);
    $employer->setPasswd($passwd);
    $employer->setNip($formData['nip']);
    $employer->setNameContactPerson($formData['name']);
    $employer->setUrl($savePath."/"."$newName");
    $employer->setAddress($formData['adress']);
    $employer->setAgreement(1);
    $employer->setPostalCode($formData['postcode']);
    $employer->setCity($formData['city']);
    $employer->setOptions('user');
    $employer->setCountry($country);

    $em = $this->getDoctrine()->getManager();

    // tells Doctrine you want to (eventually) save the Product (no queries yet)
    $em->persist($employer);

    // actually executes the queries (i.e. the INSERT query)
    $em->flush();

    

			
			
			$formData='Zapisano Dane formularza';
		}
	

		return $this->render('JoblandBundle:Blog:for-employers/register-employer.html.twig', array(
			'form' => $form->createView(),
			'formData' => isset($formData) ? $formData : NULL,
		));
	
	}
	/**
     * @Route("/aplikuj/{id}")
     *
     */
	public function applyAction($id,  Request $Request) {
		
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
	$rows = $Repo ->find($id);
	$emailto=$rows->getApplyAdress();
	$jobname=$rows->getName();



		$form = $this->createFormBuilder()
		->add('name', TextType::class)
		->add('surname', TextType::class)
		->add('email', TextType::class)
		->add('phone', TextType::class)		
		->add('cv', FileType::class)
		->add('agreement', CheckboxType::class)
		->add('newsletter', CheckboxType::class)
		->add('apply', SubmitType::class)

			->getForm();


			$form->handleRequest($Request);
			

		if($form->isValid()){

			$savePath = $this->get('kernel')->getRootDir().'/../web/uploads';
			// $em = $this->getDoctrine()->getManager();
			// $em->persist($Register);
			// $em->flush();
		$formData = $form->getData();


			unset($formData['cv']);
			$randVal = rand(1000,9999);
			
			
			$file = $form->get('cv')->getData();
			if(NULL !=$file){
				$newName = sprintf('cv__%d.%s', $randVal, $file->guessExtension());
				$file->move($savePath, $newName);
			}
$path = $savePath."/".$newName;
echo $path;


    $cvdatabase = new CvDatabase();
    $cvdatabase->setName($formData['name']);
    $cvdatabase->setSurname($formData['surname']);
    $cvdatabase->setEmail($formData['email']);
    $cvdatabase->setPhone($formData['phone']);
    $cvdatabase->setPasswd('123456');
    $cvdatabase->setUrl($path);
    $cvdatabase->setStatus('1');


    

    $em = $this->getDoctrine()->getManager();

    // tells Doctrine you want to (eventually) save the Product (no queries yet)
    $em->persist($cvdatabase);

    // actually executes the queries (i.e. the INSERT query)
    $em->flush();

     $message = \ Swift_Message::newInstance()
	->setSubject("Jobland: ".$jobname." Kandydat: ".$formData['name']." ".$formData['surname'])
	
	->setFrom($formData['email'])
	->setTo($emailto)
	->setBody("Wiadomość od: ".$formData['email']."\r\n"."Numer kontaktowy: ".$formData['phone']."\r\n"."Treść wiadomości: Otrzymałeś zgłoszenie od kandydata:".$formData['name']." ".$formData['surname'])
	->attach(Swift_Attachment::fromPath($path));
	
	
	$this->get('mailer')->send($message);



    

			
			
			$formData='Zapisano Dane formularza';
		}
	

		return $this->render('JoblandBundle:Blog:for-candidates/apply.html.twig', array(
			'form' => $form->createView(),
			'formData' => isset($formData) ? $formData : NULL,
		));
	
	
	
	}
	/**
     * @Route("/faq")
     *
     */
	public function faqAction() {
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionSubkind');
		$subkind = $Repo ->findAll();
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Question');
		$question = $Repo ->findAll();
		
		
		return $this->render('JoblandBundle:Blog:for-candidates/faq.html.twig', array(
				'subkind' => $subkind,
				'question' => $question
		));
	}
	/**
     * @Route("/porady-wskazowki")
     *
     */
	public function tipsAction() {
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionSubkind');
		$subkind = $Repo ->findAll();
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Question');
		$question = $Repo ->findAll();
		
		
		return $this->render('JoblandBundle:Blog:for-candidates/tips.html.twig', array(
				'subkind' => $subkind,
				'question' => $question
		));
	}
	
	
	
	
	
	/**
	 * @Route("/oferta-pracy/{id}")
	 *
	 */
	public function worksofferExpandAction($id, Request $request) {
	
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
		$rows = $Repo ->findById($id);
		
		
	
		return $this->render('JoblandBundle:Blog:for-candidates/works-offer-expand.html.twig', array(
				'rows' => $rows
		));
	
	}
	
	/**
	 * @Route("/news")
	 *
	 */
	public function NewsAction(Request $request) {
	
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:News');
		$rows = $Repo ->findAll();
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
				$rows,
				$request->query->get('page', 1),
				5
				);
	
	
		return $this->render('JoblandBundle:Blog:news.html.twig', array(
				'rows' => $pagination
		));
	
	}
	
	/**
	 * @Route("/admin/questions", name="admin-questions")
	 *
	 */
	public function QuestionAction(Request $request) {
	
	
			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionKind');
			$rows = $Repo ->findAll();
		
		
			$choices = array();
			foreach ($rows as $value) {
				$choices[$value->getName()] = $value->getId();
			}
		
			
		
		$questionsubkind = new QuestionSubkind;
		
		$subkindform = $this->createFormBuilder($questionsubkind)
		->add('name', TextType::class, array(
				'label' => 'Dodaj Podkategorię'))
		->add('questionkindid', ChoiceType::class ,array(
						'label' => 'Jaka podstrona',
						'choices' => $choices))
						->add('add', SubmitType::class)
						->getForm();
						$subkindform->handleRequest($request);
							
		
						if($subkindform->isValid()){
							$name=$subkindform['name']->getData();
							$questionkindid=$subkindform['questionkindid']->getData();
								
							$questionsubkind->setName($name);
							$questionsubkind->setQuestionKindId($questionkindid);
		
		
							$em = $this->getDoctrine()->getManager();
							$em->persist($questionsubkind);
							$em->flush();
		
							return $this->redirectToRoute('admin-questions');
						}
			
						$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionSubkind');
						$rows = $Repo ->findAll();
						
						
						$subkindchoices = array();
						foreach ($rows as $value) {
							$subkindchoices[$value->getName()] = $value->getId();
						}
						
		$question = new Question;
		
		$form = $this->createFormBuilder($question)
		->add('question', TextType::class, array(
				'label' => 'Dodaj Pytanie'))
				->add('content', TextAreaType::class, array(
						'label' => 'Dodaj Odpowiedź',
						'attr' => array('class' => 'ckeditor')))
		->add('questionsubkindid', ChoiceType::class ,array(
								'label' => 'Jaka kategoria',
								'choices' => $subkindchoices))
		->add('add', SubmitType::class)
				->getForm();
				$form->handleRequest($request);
					
		
				if($form->isValid()){
					$que=$form['question']->getData();
					$content=$form['content']->getData();
					$questionsubkindid=$form['questionsubkindid']->getData();
					
					$question->setQuestion($que);
					$question->setContent($content);
					$question->setQuestionSubkindId($questionsubkindid);
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionSubkind');
					$kindchoice = $Repo ->findById($questionsubkindid);
					foreach ($kindchoice as $value) {
						$kindchoices = $value->getQuestionKindId();
					}
					
					$question->setQuestionKindId($kindchoices);
					
					
				
		
					$em = $this->getDoctrine()->getManager();
					$em->persist($question);
					$em->flush();
		
					return $this->redirectToRoute('admin-questions');
				}
		
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Question');
					$rows = $Repo ->findAll();
					$rowstwo = $Repo ->findAll();
					$paginator = $this->get('knp_paginator');
					$pagination = $paginator->paginate(
							$rows,
							$request->query->get('page', 1),
							5
							);
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionSubkind');
					$subkind = $Repo ->findAll();
					$paginator = $this->get('knp_paginator');
					$paginate = $paginator->paginate(
							$rowstwo,
							$request->query->get('page', 1),
							5
							);
	
	
					return $this->render('JoblandBundle:Blog:admin/admin-question.html.twig', array(
							'rows' => $pagination,
							'subkind' => $subkind,
							'rowstwo' => $rowstwo,
							'form' => $form->createView(),
							'subkindform' => $subkindform -> createView()
					));
	
				}
	
	/**
	 * @Route("/news/{id}")
	 *
	 */
	public function NewsContentAction($id) {
	
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:News');
		$rows = $Repo ->findById($id);
	
	
		return $this->render('JoblandBundle:Blog:news-content.html.twig', array(
				'rows' => $rows
		));
	
	}
	
	/**
	 * @Route("/page/{name}")
	 *
	 */
	public function PagesAction($name) {
		
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		$rows = $Repo->findOneByName($name);		
		$val = $rows->getHomePageId();
		
		return $this->render('JoblandBundle:Blog:layout-'.$val.'.html.twig', array(
				'rows' => $rows
		));
	
	}
	
	/**
	 * @Route("/panel-pracodawcy/podglad-oferty/{id}")
	 *
	 */
	public function previewofferAction($id) {
	
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
		$rows = $Repo ->findById($id);
	
	
		return $this->render('JoblandBundle:Blog:employer-panel/preview-offer.html.twig', array(
				'rows' => $rows
				
		));
	
	}
	
	/**
     * @Route("/jak-dziala")
     *
     */
	public function howjoblandworksAction() {
	return $this->render('JoblandBundle:Blog:for-candidates/how-jobland-works.html.twig', array());
	
	}

	/**
	 * @Route("/admin/info-page", name="pageList")
	 */
	public function pageListOnAction()
	{
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		$pages = $Repo->findBy(array('isDeleted' => 0, 'userId' => $this->getUser()), array('position' => 'ASC'));
	
		$node = '';
		$info = '';
		foreach($pages as $page){
			$info[$page->getId()]=$page;
			$path = array_reverse(explode(' ',$page->getPath()),true);
	
			$code='if(!isset($node';
			foreach($path as $key=>$value){
				$code .= '[' . $value . ']';
			}
			$code .= ')) $node';
			foreach($path as $key=>$value){
				$code .= '[' . $value . ']';
			}
			$code .= '=array();';
	
			eval($code);
	
			$tree[0]=$node;
		}
	
		return $this->render('JoblandBundle:Blog:admin/pageList.html.twig', array(
				'flattenTree' => Page::buildtree($node),
				'pages'=>$info
		));
	}
	
	
	
	
	/**
	 * @Route("/strony-informacyjne/dodaj", name="addPage")
	 */
	public function addPageAction(Request $request)
	{
		//var_dump($this->getUser());
		
		$page = new Page;
		
		
		$form = $this->createForm(CreatePageType::class, $page);
		 
		$form->handleRequest($request);
		 
		if ($form->isSubmitted() && $form->isValid()) {
			
			if($page->getSlug() == NULL) {
				$page->setSlug($page->getName());
			}
			
			if ($page->getParent() == NULL) {
				$page->setMegaSlug($page->getSlug());
				$page->setParent(0);
			} else {
				if(gettype($page->getParent()) == 'object') {
					$page->setMegaSlug($page->getParent()->getMegaSlug()."/".$page->getSlug());
				}
			}
			if ($page->getKind() == NULL) {
				$page->setKind(0);
			}
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($page);
			$em->flush();
			
			$page->setOptions(Page::buildOptions($request->request->get('create_page')));
	
			
			$slug = $this->checkSlug($page);
			//var_dump($slug);
			$page->setUserId($this->getUser()->getId());
			$page->setSlug($slug);
			$em->persist($page);
			$em->flush();
	
			$this->rebuildPagesTree();
	
			return $this->redirectToRoute('pageList');
		}
	
		return $this->render('JoblandBundle:Blog:admin/addPage.html.twig', array(
				'form' => $form->createView()
		));
	}
	
	/**
	 * @Route("/strony-informacyjne/edytuj/{id}", name="editPage")
	 */
	public function editPageAction(Request $request, $id)
	{
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		$page = $Repo->find($id);
	
		if($page == NULL OR $page->getUserId() != $this->getUser()) {
			throw $this->createNotFoundException('Wystąpił błąd!');
		}
	
		$form = $this->createForm(CreatePageType::class, $page);
	
		if($request->isMethod('POST')) {
			$session = new Session();
			$form->handleRequest($request);
	
			if($form->isValid()) {
				$page->setModifiedAt(new \DateTime());
	
				$page->setOptions(Page::buildOptions($request->request->get('create_page')));
	
				// Check if slug exist
				$slug = $this->checkSlug($page);
				$page->setSlug($slug);
	
				$em = $this->getDoctrine()->getManager();
				$em->persist($page);
				$em->flush();
	
				// set path / childrens / mega-slug
				$this->rebuildPagesTree();
	
				$session->getFlashBag()->add('alert alert-success', 'Zaktualizowano rekord.');
	
				return $this->redirect($this->generateUrl('pageList'));
			} else {
				$session->getFlashBag()->add('alert alert-danger', 'Popraw błędy formularza!');
			}
		}
	
		return $this->render('JoblandBundle:Blog:admin/addPage.html.twig', array(
				'form' => $form->createView(),
				'register' => $page
		));
	}
	
	/**
	 * @Route("/strony-informacyjne/load-custom-field", name="pageCustomField")
	 */
	public function pageCustomField(Request $request)
	{
		if ( $request->isXmlHttpRequest() ) {
			$val = $request->get("id");
			$pageId = $request->get("pageId");
			if ($pageId != 0) {
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
				$page = $Repo->find($pageId);
			}
	
			if ($val != '') {
				return $this->render('JoblandBundle:Admin:template'.$val.'Form.html.twig', array(
						'register' => isset($page)? $page : ''
				));
			}
		}
		return new Response;
	}
	
	/**
	 * @Route("admin/info-page/usun/{id}", name="deletePage")
	 */
	public function deletePageAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$page = $em->getRepository('JoblandBundle:Page')->find($id);
	
		if($page == NULL) {
			throw $this->createNotFoundException('Wystąpił błąd!');
		}
		$session = new Session();
	
		$page->setIsDeleted(1);
		$em->flush();
	
		$session->getFlashBag()->add('alert alert-success', 'Poprawnie usunięto rekord.');
	
		return $this->redirect($this->generateUrl('pageList'));
	}
	
	/**
	 * @Route("/admin/info-page/position-update", name="pageListPositionUpdate")
	 */
	public function executePositionUpdateAction(Request $request)
	{
		if ( $request->isXmlHttpRequest() ) {
	
			$data = $request->get("sortedList");
			 
			$parents=array();
			foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($data), \RecursiveIteratorIterator::SELF_FIRST) as $key => $value) {
				if(isset($value['children'])){
					foreach($value['children'] as $k=>$v){
						$parents[$v['id']]=$value['id'];
					}
				}
			}
	
			$i=0;
			$positions=array();
			foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($data), \RecursiveIteratorIterator::SELF_FIRST) as $key => $value) {
				if($key==="id"){
					$i++;
					$positions[$value]=$i;
				}
			}
	
			foreach($positions as $id=>$position){
				if(!isset($parents[$id])){
					$parents[$id]=0;
				}
			}
	
			$repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
	
			foreach($parents as $id=>$parent) {
				$curPage = $repo->find($id);
				$curPage->setParent($parent);
				$curPage->setPosition($positions[$id]);
	
				$em = $this->getDoctrine()->getManager();
				$em->persist($curPage);
				$em->flush();
			}
	
			$this->rebuildPagesTree();
		}
		return new Response();
	}
	
	
	
	
	/**
	 * @Route("/cookies-accept", name="cookieConsent")
	 */
	public function cookieConsentAction(Request $request) {
		$cookies = $request->cookies;
		
		if ($cookies->has('SYMFONY2_TEST'))
		{
			var_dump($cookies->get('SYMFONY2_TEST'));
		}
	}
	
	

	/**
	 * @Route("/{slug}", name="showPage", requirements={"slug" = ".*"}) 
	 */
	public function showPageAction(Request $request, $slug)
	{
		//echo $curPage;
		//echo $slug; die();
		
		
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		$page = $Repo->findOneBy(array('megaSlug' => $slug));
		
	
		//var_dump($page);
		//die();
		
		if($page == NULL OR $page->getIsPublished() == FALSE OR $page->getIsDeleted() == 1) {
			throw $this->createNotFoundException('Wystąpił błąd!');
		}
		//echo 'test'; die();
		/*
		if($page == $page->getUserId()->getHomePageId()) {
		return $this->redirectToRoute('homepage');
		}
		*/
	
		
	
	
		if($page->getKind() == 0) {
			$pageTemplate = 'show.html.twig';
			} else if($page->getKind() == 1) {
				$parent = $page->getParent();
				
				
			
				
				
				if($parent == 0){
					$childrenIds =array();
					$children = explode(' ',$page->getChildren());
					
					
					foreach($children as $child){
						if($child){
							$childrenIds[]=$child;
					
						}
					}
					
					
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
					$sidebar = $Repo ->findById($childrenIds);
					
				}else{
				
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
					
				$sidebar = $Repo ->findByParent($parent);
				}
				
				if($parent == 0){
					
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
					
					$navi = $Repo ->findById($page);
					$naviparent = NULL;
					
				}else{
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
					$navi = $Repo ->findById($page);
					$naviparent = $Repo ->findById($parent);
				}
				$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
		
				
					
				return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
						'page' => $page,
						'sidebar' => $sidebar,
						'navi' => $navi,
						'naviparent' => $naviparent
					
							
				));
				
				
				
			
			} else if($page->getKind() == 2) {
			
				
				
			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
			
			$rows = $Repo ->findAll();
			$pl = $Repo ->findByCountryId('PL');
			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:News');
			
			$news = $Repo ->findAll();
			
			$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
			
			
			
			return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
					'page' => $page,
					'rows' => $rows,
					'news' => $news,
					'pl' => $pl
					
			));
			} else if($page->getKind() == 3) {
			
				$form = $this->createFormBuilder()
				->add('name', TextType::class)
				->add('email', EmailType::class)
				->add('phone', TextType::class)
				->add('message', TextareaType::class)
				->add('agreement', CheckboxType::class)
				->add('send', SubmitType::class)
				->getForm();
				$form->handleRequest($request);
				if($form->isValid()){
			
					$formData = $form->getData();
					$message = \ Swift_Message::newInstance()
					->setSubject($formData['name'])
					->setFrom($formData['email'])
					->setTo('damian.krawczuk@cyrek.it')
					->setBody("Wiadomość od: ".$formData['email']."\r\n"."Numer kontaktowy: ".$formData['phone']."\r\n"."Treść wiadomości: ".$formData['message']);
					$this->get('mailer')->send($message);
			
				}
				$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
				return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
						'form' => $form->createView(),
						'formData' => isset($formData) ? $formData : NULL,
						'page' => $page
				));
			
			
		} else if($page->getKind() == 4) {
			$parent = $page->getParent();
			if($parent == 0){
				$childrenIds =array();
				$children = explode(' ',$page->getChildren());
					
					
				foreach($children as $child){
					if($child){
						$childrenIds[]=$child;
							
					}
				}
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
			
				$sidebar = $Repo ->findById($childrenIds);
					
			}else{
			
			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
				
			$sidebar = $Repo ->findByParent($parent);
			}
			if($parent == 0){
					
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
					
				$navi = $Repo ->findById($page);
				$naviparent = NULL;
					
			}else{
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
			
				$navi = $Repo ->findById($page);
				$naviparent = $Repo ->findById($parent);
			}
			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Media');
			$rows = $Repo ->findAll();
			$paginator = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
					$rows,
					$request->query->get('page', 1),
					5
					);
			
			$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
				
				
				
			return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
					'rows' => $pagination,
					'page' => $page,
					'sidebar' => $sidebar,
					'navi' => $navi,
					'naviparent' => $naviparent
					
			
			));
		} else if($page->getKind() == 5) {
			
			$parent = $page->getParent();
			if($parent == 0){
				$childrenIds =array();
				$children = explode(' ',$page->getChildren());
					
					
				foreach($children as $child){
					if($child){
						$childrenIds[]=$child;
							
					}
				}
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
			
				$sidebar = $Repo ->findById($childrenIds);
					
			}else{
			
			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
			
			$sidebar = $Repo ->findByParent($parent);
			}
			if($parent == 0){
					
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
					
				$navi = $Repo ->findById($page);
				$naviparent = NULL;
					
			}else{
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
			
				$navi = $Repo ->findById($page);
				$naviparent = $Repo ->findById($parent);
			}
			
			$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Portfolio');
			$rows = $Repo ->findAll();
			$paginator = $this->get('knp_paginator');
			$pagination = $paginator->paginate(
					$rows,
					$request->query->get('page', 1),
					5
					);
			
			$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
			
			
			
			return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
					'rows' => $pagination,
					'page' => $page,
					'sidebar' => $sidebar,
					'navi' => $navi,
					'naviparent' => $naviparent
						
						
			));
			} else if($page->getKind() == 6) {
					
			
				$parent = $page->getParent();
				if($parent == 0){
					$childrenIds =array();
					$children = explode(' ',$page->getChildren());
						
						
					foreach($children as $child){
						if($child){
							$childrenIds[]=$child;
								
						}
					}
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
				
					$sidebar = $Repo ->findById($childrenIds);
						
				}else{
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
				
				$sidebar = $Repo ->findByParent($parent);
				}
				$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Partners');
				$rows = $Repo ->findAll();
				$paginator = $this->get('knp_paginator');
				$pagination = $paginator->paginate(
						$rows,
						$request->query->get('page', 1),
						9
						);
				
			
				$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
					
					
					
				return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
						'rows' => $pagination,
						'page' => $page,
						'sidebar' => $sidebar,
						'navi' => $navi,
						'naviparent' => $naviparent
			
			
				));
				} else if($page->getKind() == 7) {
						
					$parent = $page->getParent();
					if($parent == 0){
						$childrenIds =array();
						$children = explode(' ',$page->getChildren());
					
					
						foreach($children as $child){
							if($child){
								$childrenIds[]=$child;
					
							}
						}
						$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
					
						$sidebar = $Repo ->findById($childrenIds);
					
					}else{
						$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
					
						$sidebar = $Repo ->findByParent($parent);
					}
					if($parent == 0){
							
						$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
							
						$navi = $Repo ->findById($page);
						$naviparent = NULL;
							
					}else{
						$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
					
						$navi = $Repo ->findById($page);
						$naviparent = $Repo ->findById($parent);
					}
						
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:User');
					$user = $Repo ->findAll();
					
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Offer');
					$rows = $Repo ->findBy(array(
							'offerStatus' => 1
					));
					$paginator = $this->get('knp_paginator');
					$pagination = $paginator->paginate(
							$rows,
							$request->query->get('page', 1),
							5
							);
						
					$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
						
						
						
					return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
							'rows' => $pagination,
							'page' => $page,
							'user' => $user,
							'sidebar' => $sidebar,
							'navi' => $navi,
							'naviparent' => $naviparent
								
								
					));
					} else if($page->getKind() == 8) {
						
						$parent = $page->getParent();
						if($parent == 0){
							$childrenIds =array();
							$children = explode(' ',$page->getChildren());
						
						
							foreach($children as $child){
								if($child){
									$childrenIds[]=$child;
						
								}
							}
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
							$sidebar = $Repo ->findById($childrenIds);
						
						}else{
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
							$sidebar = $Repo ->findByParent($parent);
						}
						if($parent == 0){
								
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
								
							$navi = $Repo ->findById($page);
							$naviparent = NULL;
								
						}else{
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
							$navi = $Repo ->findById($page);
							$naviparent = $Repo ->findById($parent);
						}
						
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionSubkind');
					$subkind = $Repo ->findAll();
					$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Question');
					$question = $Repo ->findAll();
					
					$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
					return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
							'subkind' => $subkind,
							'page' =>$page,
							'question' => $question,
							'sidebar' => $sidebar,
							'navi' => $navi,
							'naviparent' => $naviparent
					));
					} else if($page->getKind() == 9) {
						
						$parent = $page->getParent();
						if($parent == 0){
							$childrenIds =array();
							$children = explode(' ',$page->getChildren());
						
						
							foreach($children as $child){
								if($child){
									$childrenIds[]=$child;
						
								}
							}
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
							$sidebar = $Repo ->findById($childrenIds);
						
						}else{
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
							$sidebar = $Repo ->findByParent($parent);
						}
						if($parent == 0){
								
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
								
							$navi = $Repo ->findById($page);
							$naviparent = NULL;
								
						}else{
							$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
						
							$navi = $Repo ->findById($page);
							$naviparent = $Repo ->findById($parent);
						}
						$Repo = $this->getDoctrine()->getRepository('JoblandBundle:QuestionSubkind');
						$subkind = $Repo ->findAll();
						$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Question');
						$question = $Repo ->findAll();
							
						$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
						return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
								'subkind' => $subkind,
								'page' =>$page,
								'question' => $question,
								'sidebar' => $sidebar,
								'navi' => $navi,
								'naviparent' => $naviparent
						));
						} else if($page->getKind() == 10) {
							
								
							$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
							return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
								
									'page' =>$page,
									
									
							));
				
		} else {
			$pageTemplate = 'layout-'.$page->getKind().'Success.html.twig';
		}
	
		return $this->render('JoblandBundle:Blog:Templates/'.$pageTemplate, array(
				'page' => $page,
				'userId' => $page->getUserId()
		));
	}
	
	
	private function checkSlug($page)
	{
	
		$repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		$qb = $repo->createQueryBuilder('p');
	
		$qb->where('p.id != :identifier')
		->setParameter('identifier', $page->getId());
		
		$pages = $qb->getQuery()->getResult();
	
		$page->setSlug($page->getName());
		$baseSlug = $page->getSlug();
	
		$i=1;
		foreach($pages as $val) {
			while($val->getSlug() == $page->getSlug()) {
	
				$page->setSlug($baseSlug."-".$i);
				$i++;
			}
		}
		return $page->getSlug();
	}
	
	
	public function rebuildPagesTree()
	{
		$conn = $this->get('database_connection');
		ini_set('max_execution_time', 300);
	
		$repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		
// 		$pages = $repo->findBy(array('isDeleted' => 0, 'userId' => $this->getUser()), array('parent' => 'ASC', 'position' => 'ASC'));
		$pages = $repo->findAll(array('parent' => 'ASC', 'position' => 'ASC'));
		
		$tmp=$pages;
		
		
	
		$pages=array();
	
		if(!empty($tmp))
		{
			foreach($tmp as $_tmp) { $pages[$_tmp->getId()]=$_tmp; }
		}
	
		if(!empty($pages)) {
			$q="INSERT INTO `page` (id,path,children) VALUES ";
			$tmp=array();
	
			$i=0;
			foreach($pages as $key=>$page) {
				$children = Page::buildChildren($page->getId(),$pages);
				$children = array_filter($children);
				$children = array_unique($children);
			
				
				$page->setChildren(!empty($children)?implode(" ",$children):'');
			
				$path = Page::buildPath($page->getId(),array(),$pages);
				$path=array_filter($path);
				$path=array_unique($path);
				
				
				$page->setPath(!empty($path)?implode(" ",$path):'');
		
			
				$pages[$key]=$page;
				
			
				$tmp[]=" (".$page->getId().",'".trim(implode(" ",$path))."','".trim(implode(" ",$children))."')";
				
				
			}
			
			$q.=implode(",", $tmp);
			$q.=" ON DUPLICATE KEY UPDATE path=values(path), children=values(children)";
			
			
			
			

			// insert data
			if(!empty($tmp)) { $conn->prepare($q)->execute(); }
	
			$q="INSERT INTO `page` (id,mega_slug,descendants) VALUES ";
			$tmp=array();
			$i=0;
	
			foreach($pages as $key=>$page)
			{
				$megaslug= Page::buildMegaSlug($page->getId(),explode(" ",$page->getPath()),$pages);
				$page->setMegaSlug($megaslug);
	
				$descendants= Page::buildDescendants($page->getId(),$pages);
				$descendants=array_filter($descendants);
				$descendants=array_unique($descendants);
				$page->setDescendants(!empty($descendants)?trim(implode(" ",$descendants)):'');
	
				$pages[$key]=$page;
	
				$tmp[]=" (".$page->getId().",'".$megaslug."','".trim(implode(" ",$descendants))."')";
			}
	
			$q.=implode(",", $tmp);
			$q.=" ON DUPLICATE KEY UPDATE mega_slug=values(mega_slug), descendants=values(descendants)";
	
			// insert data
			if(!empty($tmp)) { $conn->prepare($q)->execute(); }
		}
	}
	
	public function executeMenuTopAction()
	{
		$tree = array();
		$info = array();
		$childrenIds=array();
	
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		$pages = $Repo->findBy(array('isDeleted' => 0, 'isPublished' => 1, 'isFeatured' => 1), array('position' => 'ASC'));
	
		foreach($pages as $page){
			$info[$page->getId()]=$page;
			$children = explode(' ',$page->getChildren());
	
			if(!isset($tree[0][$page->getId()])){
				$tree[0][$page->getId()]=array();
			}
			foreach($children as $child){
				if($child){
					$childrenIds[]=$child;
					$tree[0][$page->getId()][$child]=array();
				}
			}
		}
	
		$Repo = $this->getDoctrine()->getRepository('JoblandBundle:Page');
		$pages = $Repo->findById($childrenIds);
	
		foreach($pages as $page){
			$info[$page->getId()]=$page;
		}
		$uri = $_SERVER['REQUEST_URI'];
	
		return $this->render('JoblandBundle:Blog:Templates/menuTop.html.twig', array(
				'pages' => $info,
				'tree' => $tree,
				'uri' => $uri
	
		));
	}
	
	
	

	

}

?>