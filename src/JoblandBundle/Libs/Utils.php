<?php

namespace JoblandBundle\Libs;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;


class Utils 
{
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    static public function slugify($text)
    { 
      // replace non letter or digits by -
      $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

      // trim
      $text = trim($text, '-');

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // lowercase
      $text = strtolower($text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      if (empty($text))
      {
        return 'n-a';
      }

      return $text;
    }
    
    static public function getHost()
    {
        $host = $_SERVER['SERVER_NAME'];
        //$host = filter_input(INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRING);
        return $host;
    }
    
    static public function extractDomain($domain)
    {   
        if(preg_match("/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i", $domain, $matches))
        {
            return $matches['domain'];
        } else {
            return $domain;
        }
    }

    static public function extractSubdomains($url)
    {
        $subdomains = $url; 
        $domain = Utils::extractDomain($url);
        
        $subdomains = rtrim(strstr($subdomains, $domain, true), '.');
        $host_Array = explode(".",$subdomains);
        
        if(isset($host_Array[0]) && $host_Array[0] == 'www') {
            array_shift($host_Array);
        }

        return implode($host_Array, ".");
    }
    
    public function getUserByDomain() 
    {
        $host = Utils::getHost();
        $subdomain = Utils::extractSubdomains($host);
        $domain = Utils::extractDomain($host);
        
       
        $em = $this->entityManager;
        $query = $em->createQuery('SELECT u FROM JoblandBundle:User u WHERE u.enabled = :enabled AND (u.subdomain = :subdomain OR u.domain = :domain)')
                ->setParameter('enabled', 1)
                ->setParameter('subdomain', $subdomain)
                ->setParameter('domain', $domain);
                
        $user = $query->getOneOrNullResult();

        return $user;
    }
    
    public function getCustomCss()
    {
        $user = $this->getUserByDomain();
        
        $em = $this->entityManager;
        $query = $em->createQuery('SELECT c FROM JoblandBundle:Config c JOIN JoblandBundle:User u WHERE c.name = :customCss AND c.userId = :userId')
        ->setParameter('userId', $user)
        ->setParameter('customCss', 'customCss');
        
        $css = $query->getOneOrNullResult()->getValue();
        return $css;
    }
    
    public function getTemplates() 
    {
        $templatePath = '../src/JoblandBundle/Resources/views/Blog/Templates';

        
        $files=array();
        $templates=array();
        
        if ($handle = opendir($templatePath)) {
            while (false !== ($entry = readdir($handle))) {
                
                preg_match("/layout-[0-9]Success\.html.twig/",$entry,$match);
                if($match){
                    $files[]=$match[0];
                }
            }
            closedir($handle);
        }
        
        
        foreach($files as $file){
            $content=file($templatePath.'/'.$file);
            preg_match("/layout-([0-9])Success\.html.twig/",basename($file),$match);
            $id = $match[1];
        
            if(isset($content[0])){
                //preg_match("/\*(.*)\*/",$content[0],$match);
                preg_match("/{#(.*)#}/",$content[0],$match);
                if(isset($match[1])){
                    $templates[$id]=$match[1];
                }
        
            }
        }
        
        return $templates;
    }
    
    public function getAllFunctions()
    {
        $function = array(
            'submit_property' => 'zgłoś ofertę',
            'send_requirement' => 'zgłoś czego szukasz', 
            'properties_map' => 'prezentacja ofert na mapie', 
            'properties_homepage' => 'oferty specjalne (na stronie głównej)', 
            'properties_favorites' => 'dodawanie ofert do notesu (ulubione, gwiazdka)', 
            'page_agents' => 'możliwość dodania podstrony "pośrednicy"', 
            'page_featured' => 'możliwość dodania strony "oferty promowane"', 
            'search_advanced' => 'wyszukiwarka zaawansowana ofert', //- nr oferty, licz. Pok., piętro, licz. Pięter, itp
            'cost_calculator' => 'kalkulator kosztów przy ofercie', //- taksa notarialna, prowizja agencji, podatek
            'enable_share' => 'integracja z portalami (udostępnianie)',
            'properties_newest' => 'najnowsze oferty', 
            'credit_calculator' => 'kalkulator kredytowy',
            'properties_sold' => 'oferty sprzedane - przekreślona cena', 
            'dynamic_header' => 'dynamiczny baner na stronie głównej', //- (zmieniające się oferty lub zdjęcia w tle)
            'page_statistics' => 'statystyki odwiedzin strony', 
            'points_of_interest' => 'punkty w okolicy', //- przedszkola, szkoły, bankomaty, apteki, przystanki, itp.
            'properties_nearby' => 'oferty w okolicy', //- pokazwyanie na mapie innych ofert w okolicy
            'properties_similar' => 'oferty podobne',
            'select_listing_type' => 'wybór formy listingu'
        );
        return $function;
    }
    
    public function isFunctionAvailable($var = '')
    {
        if ($var != '') {
            $function = $this->getAllFunctions();
            if(array_key_exists($var,$function)) {
                $user = $this->getUserByDomain();
                if(!empty($user)) {
                    $em = $this->entityManager;
                    $query = $em->createQuery('SELECT c FROM JoblandBundle:Config c WHERE c.name = :name AND c.userId = :uId')
                        ->setParameter('name', 'functionsAvailable')
                        ->setParameter('uId', $user);
                    
                    $functionsAvailable = $query->getOneOrNullResult();
                    if(!empty($functionsAvailable)) {
                        $functionsAvailableArray = json_decode($functionsAvailable->getValue(),true);
                        if($functionsAvailableArray[$var] == 1) {
                            $query = $em->createQuery('SELECT c FROM JoblandBundle:Config c WHERE c.name = :name AND c.userId = :uId')
                                ->setParameter('name', 'functionsEnabled')
                                ->setParameter('uId', $user);
                            
                            $functionsEnabled = $query->getOneOrNullResult();
                            if(!empty($functionsEnabled)) {
                            $functionsEnabledArray = json_decode($functionsEnabled->getValue(),true);
                                if($functionsEnabledArray[$var] == 1) {
                                    return true;
                                }
                            }
                        }
                    }                    
                }
            }            
        }
        return false;
    }
    
    
    public function getTypes()
    {
        $types = array(
                1 => 'mieszkanie',
                2 => 'dom',
                3 => 'działka',
                4 => 'komercyjne',
                5 => 'garaż',
                6 => 'pokój'
                /*
                7 => '',
                8 => '',
                9 => '',
                10 => '',
                11 => '',
                12 => '',
                13 => '',
                14 => '',
                15 => '',
                16 => '',
                17 => '',
                18 => '',
                19 => '',
                20 => '',
                21 => '',
                22 => '',
                23 => '',
                24 => '',
                25 => '',
                26 => '',
                27 => '',
                28 => '',
                29 => '',
                30 => '',
                31 => '',
                32 => '',
                33 => '',
                34 => '',
                35 => '',
                36 => ''
                */
        );
        return $types;
    
    }

}