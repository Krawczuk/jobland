<?php

namespace JoblandBundle\Libs;

use Doctrine\ORM\EntityManager;
use JoblandBundle\Libs\Utils;

class Shortcode 
{
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    public function shortcode($var = '') 
    {
        $host = Utils::getHost();
        $subdomain = Utils::extractSubdomains($host);
        $domain = Utils::extractDomain($host);
        
        if($var != '') {
            $em = $this->entityManager;
        
            $query = $em->createQuery('SELECT c FROM JoblandBundle:Config c JOIN JoblandBundle:User u WHERE c.name = :name AND c.userId = u.id AND (u.subdomain = :subdomain OR u.domain = :domain)')
                    ->setParameter('subdomain', $subdomain)
                    ->setParameter('domain', $domain)
                    ->setParameter('name', 'shortcode');

            $config = $query->getOneOrNullResult();
            $options = json_decode($config->getValue(), true);
            foreach($options as $key => $opt) {
                if($opt['name'] == $var) {
                    return $options[$key]['content'];
                }
            }
        }
        
        return '';
    }
}