<?php
/*
Tryby generowania: $mode;

    1 - center: zdjęcie jest skalowane tak aby w całości zmieściło się w podanych wymiarach, dodatkowo dodawane jest białe tło na obszarze nie zajętym przez zdjęcie tak aby uzyskać wymagany rozmiar
    2 - normal: zdjęcie jest skalowane tak aby w całości zmieściło się w podanych wymiarach (zdjęcie może mieć mniejsze wymiary niż podane, a dokładniej jeden z nich)
    4 - crop: zdjęcie jest skalowane tak aby w całości wypełnić zadeklarowany obszar (część zdjęcia zostanie obcięta)
    8 - stretch: zdjęcie jest skalowane tak aby w całości wypełnić zadeklarowany obszar (uwaga ten tryb nie zachowuje proporcji zdjęć!)
    16 - shrink: tryb działa analogicznie jak `normal` z wyjątkiem tego że nie powiększa zdjęcia (nie uzyskamy miniaturki większej niż oryginał)
    32 - crop_shrink: tryb specjalnie stworzony dla zdjęć umieszczanych na Facebooku. W skrócie działa jak tryb `crop` z tym że nie powiększa oryginału (jak tryb `shrink`).
*/

namespace JoblandBundle\Libs;

//use Doctrine\ORM\EntityManager;
use JoblandBundle\Libs\Utils;

class PropertyThumb
{
    /*
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    */
    public function thumb($url, $width, $height, $mode = 4) 
    {
        $availableMode = array(1, 2, 4, 8, 16, 32);
        if(!in_array($mode, $availableMode)) {
            return 'Wybrano niepoprawny moduł';
        }
        
        $serverNr = (hexdec(substr(md5($url), 0, 2)) % 3) + 1;
        $baseUrl = base64_encode($url);
        $filename = explode('/', $url);
        $filename = end($filename);
        $filename = preg_replace("/\#.+/", "", $filename);
        $imgName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $imgName = Utils::slugify($imgName);
        $ext = 'jpg';
        
        $thumb = 'http://'.'img'.$serverNr.'.staticmorizon.com.pl'.'/thumbnail/'.$baseUrl.'/'.$width.'/'.$height.'/'.$mode.'/'.$imgName.'.'.$ext;
        
        //return '<img src="'.$thumb.'">';
        return $thumb;
        // usage
        // {{ thumb.thumb('http://media.domy.pl/img/deweloperzy/4060_wizualizacja7.jpg', 321, 241) }}
    }
}