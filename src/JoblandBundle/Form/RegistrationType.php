<?php


namespace JoblandBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('company');
        $builder->add('phoneNumber');
        $builder->add('nip');
        $builder->add('contactPerson');
        $builder->add('url');
        $builder->add('profilePictureFile', FileType::class);
        $builder->add('address');
        $builder->add('postalCode');
        $builder->add('city');
        $builder->add('countryId',CountryType::class);
        $builder->add('agreement', CheckboxType::class);
        $builder->add('rules', CheckboxType::class);
        
        
       
        
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
} 