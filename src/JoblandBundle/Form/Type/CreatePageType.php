<?php

namespace JoblandBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
//use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JoblandBundle\Libs\Utils;
use JoblandBundle\Entity\Page;

class CreatePageType extends AbstractType
{

	//     private $tokenStorage;
	private $utils;

	//public function __construct(TokenStorageInterface $tokenStorage, Utils $utils)
	public function __construct(Utils $utils)
	{
		//$this->tokenStorage = $tokenStorage;
		$this->utils = $utils;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		
		
		$currentDescendants = 0;
		if($options['data']->getDescendants() != NULL) {
			$currentDescendants = explode(' ', $options['data']->getDescendants());
			$currentDescendants = implode(',', $currentDescendants);
		}

		$currentParent = 0;
		if($options['data']->getParent() != NULL) {
			$currentParent = $options['data']->getParent();
		}

		$currentKind = 0;
		if($options['data']->getKind() != NULL) {
			$currentKind = $options['data']->getKind();
		}

		$userByDomain = $this->utils->getTemplates();
		$templates = array_flip($userByDomain);

		$builder
		->add('name', TextType::class)
		->add('nameMenu', TextType::class)
		->add('content', TextareaType::class, array(
				'required' => FALSE,
				'attr' => array('class' => 'ckeditor')
		))
		->add('language', ChoiceType::class, array(
    'choices' => array(
        'Polski' => 'PL',
        'English' => 'EN',
       
    ),
				'required' => TRUE
		))
		->add('externalUrl', TextType::class, array(
				'required' => FALSE
		))
		->add('isPublished', CheckboxType::class, array(
				'label' => 'Strona opublikowana',
				'required' => FALSE
		))
		
		->add('isFeatured', CheckboxType::class, array(
				'label' => 'Strona widoczna w menu głównym',
				'required' => FALSE
		))
		//            ->add('metaTitle', TextType::class, array(
				//                'mapped' => FALSE,
				//                'required' => FALSE
				//                ))
		//            ->add('metaDescription', TextType::class, array(
				//                'mapped' => FALSE,
				//                'required' => FALSE
				//                ))
				//            ->add('metaKeywords', TextType::class, array(
						//                'mapped' => FALSE,
						//                'required' => FALSE
						//                ))
		
		
		->add('kind', ChoiceType::class, array(
				'label' => 'Szablon',
				'placeholder' => 'Szablon Domyślny',
				'empty_data' => NULL,
				'required' => FALSE,
				'choices' => $templates,
				'choice_attr' => function($p) use ( $currentKind ){
				if($p == $currentKind) {
					return ['selected' => 'selected'];
				} else {
					return [];
				}
				},
				))
				->add('parent', EntityType::class, array(
						'class' => 'JoblandBundle:Page',
						'label' => 'Strona nadrzędna',
						'required' => FALSE,
						'placeholder' => '--Kategoria główna--',
						'empty_data' => NULL,
						'choice_label' => function ($p) {
						$ar = count(explode(' ', $p->getPath()))-1;
						$string = '';
						for($i = 0; $i < $ar; $i++) {
							$string .='&nbsp;&nbsp;&nbsp;&nbsp;';
						}
						return html_entity_decode($string).$p->getName();
						},
						'choice_attr' => function($p) use ( $currentParent ) {
						if($currentParent == $p->getId()) {
							return ['selected' => 'selected'];
						} else {
							return [];
						}
						},
						'query_builder' => function (EntityRepository $er) use ( $currentDescendants ) {
						return $er->createQueryBuilder('p')
						->where('p.isDeleted = 0 AND p.id NOT IN ('.$currentDescendants.')')
						->orderBy('p.position', 'ASC');
						},
						))
						->add('save', SubmitType::class, array(
								'label' => 'Zapisz',
								'attr' => array('class' => 'btn btn-success')
						))
						;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
				'allow_extra_fields' => true
		));
	}
}